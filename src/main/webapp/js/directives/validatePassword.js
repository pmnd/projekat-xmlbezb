(function (angular) {
    "use strict";

    angular.directive('validatePassword', function () {
      return {
        require: 'ngModel',
        scope: {

          reference: '=validatePassword'

        },
        link: function(scope, elm, attrs, ctrl) {
          ctrl.$parsers.unshift(function(viewValue, $scope) {

            var noMatch = viewValue != scope.reference
            ctrl.$setValidity('noMatch', !noMatch);
            return (noMatch)?noMatch:!noMatch;
          });

          scope.$watch("reference", function(value) {;
            ctrl.$setValidity('noMatch', value === ctrl.$viewValue);

          });
        }
      }
    });
}(angular));