(function (angular){
    "use strict";

    angular.module('app.pretragaController', [])
        .controller('pretragaController', function ($scope, Session, $routeParams, $location, pretragaService, $sce) {
            
            $scope.role = Session.getUser().role;
            $scope.searchCase = $routeParams.param;
            $scope.resultsReady = false;
            $scope.element = "";
            $scope.searchValue = "";
            $scope.tableName = "Pretraga"

            switch ($scope.searchCase) {
                case 'ps':

                    break;

                 case 'pr':
                    
                    break;

                 case 'as':
                    
                    break;

                 case 'ar':
                    
                    break;
            
                default:
                    $location.path('/home');
                    break;
            }

            $scope.search = function(){
                if($scope.searchValue == undefined || $scope.searchValue == ''){
                    alert("Morate popuniti polje.");
                    return;
                }
                switch ($scope.searchCase) {
                case 'ps':
                    pretragaService.searchByValue('propis',$scope.searchValue).success(function(data){
                        $scope.resultsReady = true;
                        var temp = data;
                        for(var i in temp){
                            for(var j in temp[i].matchResults){
                                temp[i].matchResults[j] = $sce.trustAsHtml(temp[i].matchResults[j]);
                            }
                        }
                        $scope.results = temp;
                    });
                    break;

                case 'pr':
                 if($scope.element == undefined || $scope.element == ''){
                    alert("Morate odabrati element.");
                    return;
                 }
                    pretragaService.searchByElement('propis',$scope.element,$scope.searchValue).success(function(data){
                        $scope.resultsReady = true;
                        var temp = data;
                        for(var i in temp){
                            for(var j in temp[i].matchResults){
                                temp[i].matchResults[j] = $sce.trustAsHtml(temp[i].matchResults[j]);
                            }
                        }
                        $scope.results = temp;
                    });
                    break;

                case 'as':
                   pretragaService.searchByValue('amandman',$scope.searchValue).success(function(data){
                        $scope.resultsReady = true;
                        var temp = data;
                        for(var i in temp){
                            for(var j in temp[i].matchResults){
                                temp[i].matchResults[j] = $sce.trustAsHtml(temp[i].matchResults[j]);
                            }
                        }
                        $scope.results = temp;
                    });
                    break;

                case 'ar':
                    pretragaService.searchByElement('amandman',$scope.element,$scope.searchValue).success(function(data){
                        $scope.resultsReady = true;
                        var temp = data;
                        for(var i in temp){
                            for(var j in temp[i].matchResults){
                                temp[i].matchResults[j] = $sce.trustAsHtml(temp[i].matchResults[j]);
                            }
                        }
                        $scope.results = temp;
                    });
                    break;
            
                default:
                    $location.path('/home');
                    break;
                }
            }

            $scope.show = function(id){
                if($scope.searchCase == 'ps' || $scope.searchCase == 'pr')
                    $location.path('/predlozeniPropisi/' + id);
                else
                    $location.path('/predlozeniAmandmani/' + id);
            }

            $scope.cancel = function(){
                $scope.resultsReady = !$scope.resultsReady;
            }
            
        });
    
}(angular));