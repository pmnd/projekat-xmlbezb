(function (angular){
    "use strict";

    angular.module('app.mainController', [])
        .controller('mainController', function ($scope, Session) {
            $scope.role = Session.getUser().role;
            
        });
    
}(angular));