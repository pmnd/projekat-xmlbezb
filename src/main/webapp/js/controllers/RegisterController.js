(function (angular) {
    "use strict";

    angular.module('app.registerController', [])
        .controller('registerController', function ($scope, $rootScope, $location, authentificationService, Session) {
            
            $scope.passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
            $scope.emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        
            $scope.registerUser = function (user) {
                $rootScope.spinner.active = true;
                authentificationService.register(user).success(function (res) {
                        if(res.status == 200){
                            //stavljamo token u session servis
                            Session.setUser({"token" : res.metadata.Authorization[0], "role": res.metadata.Role[0]});
                            $location.path("/home");
                        }
                        else if(res.status == 406){
                            $scope.errorMessage = "Email koji ste uneli već postoji. Unesite novi.";
                        }
                        else{
                            $scope.errorMessage = "Pogresno uneti parametri. Pokusajte ponovo.";
                        }
                    
                        $rootScope.spinner.active = false;
                    }).error(function(err){
                        //$scope.errorMessage = "Pogresno uneti parametri. Pokusajte ponovo.";
                    	$rootScope.spinner.active = false;
                    });
                
            };
        });
}(angular));