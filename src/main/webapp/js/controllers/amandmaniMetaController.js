(function (angular){
    "use strict";

    angular.module('app.amandmaniMetaController', [])
        .controller('amandmaniMetaController', function ($scope, Session, $location, amandmaniService) {
            $scope.role = Session.getUser().role;
            

            $scope.searchParam = {};
            $scope.result = {};
            $scope.dataReady = false;

            $scope.search = function(){
                var temp;
                temp = {
                    'datumPredlaganja':$scope.searchParam.datumPredlaganja? $scope.searchParam.datumPredlaganja.yyyymmdd(): '',
                    'naslov':$scope.searchParam.naslov ? $scope.searchParam.naslov: ''
                    };

                amandmaniService.searchMeta(temp).success(function(resp){
                        $scope.result = resp;
                        $scope.dataReady = true;
                });
            }

            $scope.showItem= function(id){
                $location.path('/predlozeniAmandmani/' + id);
            }

            
        });
    
}(angular));