(function (angular){
    "use strict";

    angular.module('app.noviAmandmanController', [])
        .controller('noviAmandmanController', function ($scope, $rootScope, amandmaniService, $location, Session) {
        $scope.role = Session.getUser().role;
        if($scope.role=='gradjanin')
            $location.path('/home');
        $scope.amandman = "";

        $scope.create = function(){
                $rootScope.spinner.active = true; 
                amandmaniService.create($scope.amandman).success(function(resp){
                    $scope.errorMesage = "";
                    alert("Uspesno ste dodali amandman.");
                    $scope.amandman = "";
                    $rootScope.spinner.active = false; 
                })
                .error(function(resp){
                    $scope.errorMesage = "Amandman nije unet u dobrom formatu. Pokušajte ponovo.";
                    $rootScope.spinner.active = false;
                });
        }
            
            
        });
    
}(angular));