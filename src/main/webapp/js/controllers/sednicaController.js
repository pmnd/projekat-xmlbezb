(function (angular){
    "use strict";

    angular.module('app.sednicaController', [])
        .controller('sednicaController', function ($scope, $rootScope, $location, sednicaService, Session) {
            $scope.role = Session.getUser().role;
            if($scope.role!='predsednik')
                $location.path('/home');
            
            $rootScope.spinner.active = true;
            $scope.stanjePropisa = [];
            sednicaService.isSednicaZapoceta().success(function(data){
                $scope.sednicaZapoceta = data;
                
                if(data){
                    $rootScope.spinner.active = true;
                    sednicaService.getPredlozeniPropisi().success(function(data){
                        
                        sednicaService.getUsvojeniPropisi().success(function(dataUsvojeni){
                            $scope.predlozeniPropisi = [];
                            for (var i = 0; i < data.length; i++) {
                                if(dataUsvojeni[i].disabled){
                                    data[i].usvojenBool = true;
                                }
                                $scope.predlozeniPropisi.push(data[i]);
                            }
                            
                            $scope.stanjePropisa = dataUsvojeni;
                            $rootScope.spinner.active = false;
                        }); 
                    });  
                }
                $rootScope.spinner.active = false;
            });
        
            $scope.pregledAmandmanaAktuelan = false;
            $scope.indexPropisa = -1;    
            $scope.nazivPropisa = "";
            
            $scope.zapocniSednicu = function(){
                $rootScope.spinner.active = true;
                sednicaService.sednicaZapoceta(true).success(function(data){
                    
                    sednicaService.getPredlozeniPropisi().success(function(data){
                        
                        $scope.predlozeniPropisi = [];
                        for (var i = 0; i < data.length; i++) {
                            $scope.predlozeniPropisi.push(data[i]);
                        }
                        
                        sednicaService.getUsvojeniPropisi().success(function(data){
                            $scope.stanjePropisa = data;
                            $rootScope.spinner.active = false;
                        }); 
                    });
                });
                
                $scope.sednicaZapoceta = true;
                
            }
            
            $scope.zavrsiSednicu = function(){
                $rootScope.spinner.active = true;
                sednicaService.sednicaZapoceta(false).success(function(data){
                    
                    var usvojeniPropisi = [];
                    for(var i = 0; i < $scope.predlozeniPropisi.length; i++){
                        if($scope.predlozeniPropisi[i].usvojenBool){
                            usvojeniPropisi.push($scope.predlozeniPropisi[i]);
                        }
                    }
                    sednicaService.zavrsiSednicu(usvojeniPropisi).success(function(data){
                        $rootScope.spinner.active = false;
                        $location.path("/home");
                    });
                });
            }
            
            $scope.pogledajAmandmane = function(id, index){
                $rootScope.spinner.active = true;
                $scope.indexPropisa = index;
                $scope.idPropisa = $scope.predlozeniPropisi[index].id;
                $scope.nazivPropisa = $scope.predlozeniPropisi[index].naziv;

                sednicaService.getAmandmani(id).success(function(data){
                    $scope.odabraniAmandmani = data;                    
                    $scope.pregledAmandmanaAktuelan = true;
                    $rootScope.spinner.active = false;
                });
               
            }
            
            $scope.povratakNaSednicu = function(){
               $scope.pregledAmandmanaAktuelan = false;
            }
            
            $scope.showItem= function(id){
                $location.path('/predlozeniAmandmani/' + id);
            }
            
            $scope.sacuvajIzmeneAmandmana = function(){
                $rootScope.spinner.active = true;
                sednicaService.updateAmandmani($scope.odabraniAmandmani).success(function(data){
                    $scope.pregledAmandmanaAktuelan = false;
                    $scope.predlozeniPropisi[$scope.indexPropisa].disabled = true;
                    
                    sednicaService.usvojiPropis($scope.idPropisa).success(function(data){
                        $scope.stanjePropisa = data;
                        $rootScope.spinner.active = false;
                    });            
                });
                
            }
            
        });
    
}(angular));