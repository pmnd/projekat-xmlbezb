(function (angular){
    "use strict";

    angular.module('app.predlozeniAmandmaniController', [])
        .controller('predlozeniAmandmaniController', function ($scope, $rootScope, $location,  amandmaniService, sednicaService, Session, $sce) {
            $scope.nazivTabele = "Predloženi amandmani"
            $scope.role = Session.getUser().role;
            $scope.user = Session.getUser().token;

            $scope.sednicaZapoceta = true;
            $scope.predlozeniAmandmani = [];
            $rootScope.spinner.active = true; 
            
            sednicaService.isSednicaZapoceta().success(function(data){
                $scope.sednicaZapoceta = data;
                amandmaniService.getAll().success(function(data) {
                    $scope.predlozeniAmandmani = data; 
                    $rootScope.spinner.active = false; 
                });
            });
            
            $scope.showItem= function(id){
                $location.path('/predlozeniAmandmani/' + id);
            }

            $scope.deleteItem= function(id){
                amandmaniService.remove(id).success(function(data){
                    alert("Uspesno ste obrisali amandman.");
                });
            }

            $scope.generatePDF = function(id){
                $rootScope.spinner.active = true; 
                amandmaniService.generatePDF(id).success(function(data){
                    var file = new Blob([data], {type: 'application/pdf'});
                    
                    var fileURL = URL.createObjectURL(file);
                    $scope.content = $sce.trustAsResourceUrl(fileURL);
                    $rootScope.spinner.active = false;
                });
            }

            $scope.createNew = function(){
                $location.path('/noviAmandman');
            }
            
        });
    
}(angular));