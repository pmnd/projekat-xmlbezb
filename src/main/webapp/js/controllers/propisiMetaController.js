(function (angular){
    "use strict";

    angular.module('app.propisiMetaController', [])
        .controller('propisiMetaController', function ($scope,$filter, Session, propisiService,$location) {
            $scope.role = Session.getUser().role;
            
            $scope.searchParam = {};
            $scope.result = {};
            $scope.dataReady = false;

            $scope.search = function(){
                var temp;
                temp = {
                    'datumUsvajanja':$scope.searchParam.datumUsvajanja? $scope.searchParam.datumUsvajanja.yyyymmdd(): '',
                    'datumPredlaganja':$scope.searchParam.datumPredlaganja? $scope.searchParam.datumPredlaganja.yyyymmdd(): '',
                    'naslov':$scope.searchParam.naslov ? $scope.searchParam.naslov: ''
                    };

                propisiService.searchMeta(temp).success(function(resp){
                        $scope.result = resp;
                        $scope.dataReady = true;
                });
            }

            $scope.showItem= function(id){
                $location.path('/predlozeniPropisi/' + id);
            }

            
        });
    
}(angular));