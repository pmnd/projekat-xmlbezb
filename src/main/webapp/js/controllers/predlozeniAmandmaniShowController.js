(function (angular){
    'use strict';

    angular.module('app.predlozeniAmandmaniShowController', ['ngSanitize'])
        .controller('predlozeniAmandmaniShowController', function ($scope, $rootScope,  amandmaniService,  $routeParams, $sce, Session) {
            $scope.role = Session.getUser().role;
            var id = $routeParams.idPropisa;
            $rootScope.spinner.active = true; 
            
            amandmaniService.getOne(id).success(function(data) {
               $scope.htmlToShow = $sce.trustAsHtml(data); 
               $rootScope.spinner.active = false; 
            });

           
        });
    
}(angular));