(function (angular){
    'use strict';

    angular.module('app.predlozeniPropisShowController', ['ngSanitize'])
        .controller('predlozeniPropisShowController', function ($scope, $location, $rootScope, propisiService, $routeParams, $sce, Session) {
            $scope.role = Session.getUser().role;           
            var id = $routeParams.idPropisa;
            $rootScope.spinner.active = true; 
            
            propisiService.getOne(id).success(function(data) {
               $scope.htmlToShow = $sce.trustAsHtml(data); 
               $rootScope.spinner.active = false;         
            });

           
        });
    
}(angular));

