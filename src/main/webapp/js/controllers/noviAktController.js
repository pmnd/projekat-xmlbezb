(function (angular){
    "use strict";

    angular.module('app.noviAktController', [])
        .controller('noviAktController', function ($scope, $rootScope, propisiService, $location, Session) {
        $scope.role = Session.getUser().role;
        if($scope.role=='gradjanin')
            $location.path('/home');

        $scope.akt = "";

        $scope.create = function(){
            $rootScope.spinner.active = true;
                propisiService.create($scope.akt).success(function(resp){
                    $scope.errorMessage = "";
                    alert("Uspesno ste dodali akt.");
                    $scope.akt = "";
                    $rootScope.spinner.active = false;
                })
                .error(function(resp){
                    $scope.errorMessage = "Propis nije unet u dobrom formatu. Pokušajte ponovo.";
                    $rootScope.spinner.active = false;
                });

        }
            
            
        });
    
}(angular));