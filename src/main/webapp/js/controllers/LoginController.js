(function (angular) {
    "use strict";
    angular.module('app.loginController', [])
        .controller('loginController',function ($cookies, $scope, $location, authentificationService, Session) {
        
            $scope.login = function (user) {
                if(user.username == 'undefined' || user.password == 'undefined'){
                    return;
                }else{
                    authentificationService.login(user).success(function (res) {
                        if(res.status == 200){
                            var obj = {"token" : res.metadata.Authorization[0], "role": res.metadata.Role[0], "email" : user.username};
                            Session.setUser(obj);
                            $scope.loginError = "";
                            $location.path("/home");
                        }else if(res.status == 403){
                            authentificationService.nextLoginDate().success(function(res){
                                $scope.errorMesage = "Prekršili ste broj dozvoljenih pokušaja prilikom logovanja. Ponovo možete pokušati da se ulogujete u: " + res; 
                            });                       
                        }
                        else{
                            authentificationService.numberOfRestAttempts().success(function(res){
                                $scope.errorMesage = "Pogrešno unet email ili lozinka. Preostali broj pokušaja: " + res;
                            });
                            
                        }
                    }).error(function(err){
                    	
                    });
                        
                   
                }
            }
            
            
            
            
        });
}(angular));