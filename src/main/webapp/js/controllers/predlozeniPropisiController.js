(function (angular){
    "use strict";

    angular.module('app.predlozeniPropisiController', [])
        .controller('predlozeniPropisiController', function ($scope, $rootScope, propisiService, sednicaService, $location, Session, $sce) {
            $scope.role = Session.getUser().role;
            $scope.user = Session.getUser().token;
            $scope.nazivTabele = "Predloženi propisi"
            $scope.sednicaZapoceta = true;
            $scope.predlozeniPropisi = [];
            $rootScope.spinner.active = true; 

            sednicaService.isSednicaZapoceta().success(function(data){
                $scope.sednicaZapoceta = data;
                propisiService.getAll('predlozen').success(function(data) {
                    $scope.predlozeniPropisi = data;
                    $rootScope.spinner.active = false; 
                });
            });   
        
            

            $scope.showItem= function(id){
                $location.path('/predlozeniPropisi/' + id);
            }

            $scope.deleteItem= function(id){
                propisiService.remove(id).success(function(data){
                    alert("Uspesno ste obrisali propis.");
                });
            }

            $scope.generatePDF = function(id){
                $rootScope.spinner.active = true; 
                propisiService.generatePDF(id).success(function(data){
                    console.log(data);
                    var file = new Blob([data], {type: 'application/pdf'});
                    var fileURL = URL.createObjectURL(file);
                    $scope.content = $sce.trustAsResourceUrl(fileURL);
                    $rootScope.spinner.active = false; 
                });
            }

            $scope.createNew = function(){
                $location.path('/noviAkt');
            }
            
        });
    
}(angular));