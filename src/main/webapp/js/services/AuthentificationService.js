(function (angular) {
    "use strict";
    angular.module('app.authentificationService', [])
        .factory('authentificationService', function ($http, Session) {
        
        
        var authService = {};

        authService.login = function (credentials) {            
            return $http.post('https://localhost:8080/rest/korisnik/login', credentials);
        };
        
        authService.register = function (user) {            
            return $http.post('https://localhost:8080/rest/korisnik/register', user);
        };

    
        
        authService.getUser = function(){
            return $http.get("https://localhost:8080/rest/korisnik")
        };
        
        authService.logout = function(){
            var req = {
            method: 'POST',
            url: 'https://localhost:8080/rest/korisnik/logout',
            headers: {
            'Authorization': Session.getUser().token
            }
            }

            return $http(req);
        }
        
        authService.numberOfRestAttempts = function(){
            return $http.get('https://localhost:8080/rest/korisnik/numberOfRestAttempts');
        }
        
        authService.nextLoginDate = function(){
            return $http.get('https://localhost:8080/rest/korisnik/nextLoginDate');
        }
        
        

        
        return authService;
    });
    
    
}(angular));