(function (angular) {
    "use strict";
    angular.module('app.sednicaService', [])
        .factory('sednicaService', function ($http, Session) {
        
        
        var sednicaService = {};
        
        sednicaService.getPredlozeniPropisi = function () {       
            var req = {
                method: 'GET',
                url: 'https://localhost:8080/rest/propis/collection/predlozen',
                headers: {
                    'Authorization': Session.getUser().token
                }
            }

            return $http(req);
        };
        
        sednicaService.getAmandmani = function (id) {       
            var req = {
                method: 'GET',
                url: 'https://localhost:8080/rest/amandman/propis/' + id,
                headers: {
                    'Authorization': Session.getUser().token
                }
            }

            return $http(req);
        };
        
        
        sednicaService.updateAmandmani = function (amandmani) {       
            var req = {
                method: 'PUT',
                url: 'https://localhost:8080/rest/amandman/updateList',
                headers: {
                    'Authorization': Session.getUser().token
                },
                data: amandmani
            }

            return $http(req);
        };
        
        sednicaService.sednicaZapoceta = function (flag) {       
            var req = {
                method: 'POST',
                url: 'https://localhost:8080/rest/sednica/sednicaZapoceta',
                headers: {
                    'Authorization': Session.getUser().token
                },
                data: flag
            }

            return $http(req);
        };
        
        sednicaService.isSednicaZapoceta = function () {       
            var req = {
                method: 'GET',
                url: 'https://localhost:8080/rest/sednica/sednicaZapoceta',
                headers: {
                    'Authorization': Session.getUser().token
                }
            }

            return $http(req);
        };
        
        sednicaService.usvojiPropis = function (id) {       
            var req = {
                method: 'POST',
                url: 'https://localhost:8080/rest/propis/usvojeniPropisi/' + id,
                headers: {
                    'Authorization': Session.getUser().token
                }
            }

            return $http(req);
        };
        
        sednicaService.getUsvojeniPropisi = function () {       
            var req = {
                method: 'GET',
                url: 'https://localhost:8080/rest/propis/usvojeniPropisi',
                headers: {
                    'Authorization': Session.getUser().token
                }
            }

            return $http(req);
        };
        
        
        sednicaService.zavrsiSednicu = function (usvojeniPropisi) {       
            var req = {
                method: 'POST',
                url: 'https://localhost:8080/rest/propis/sednica',
                headers: {
                    'Authorization': Session.getUser().token
                },
                data: usvojeniPropisi
            }

            return $http(req);
        };
        
        return sednicaService;
    });
    
    
}(angular));