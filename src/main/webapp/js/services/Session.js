(function (angular) {
    "use strict";
    angular.module('app.Session', [])
        .factory('Session', function($cookies){

            return{
                setUser : function(aUser){
                    $cookies.put("token", aUser.token);
                    $cookies.put("role", aUser.role);
                    $cookies.put("email", aUser.email);
                },
                getUser : function(){
                    return {'token': $cookies.get("token"), 'role':$cookies.get("role"), 'email':$cookies.get("email")};
                },
                
                destroyUser : function(){
                  $cookies.remove("token");
                  $cookies.remove("role");
                  $cookies.remove("email");
                },
                
                isLoggedIn : function(){
                    if($cookies.get("token") != undefined && $cookies.get("role") != undefined && $cookies.get("email") != undefined)
                        return true;
                    else
                        return false;
                }
              }
            });    
    
}(angular));