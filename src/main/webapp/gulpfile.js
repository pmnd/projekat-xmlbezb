var gulp = require('gulp');
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
 
gulp.task('compress', function() {
  gulp.src('js/**/*.js')
    .pipe(minify({
        ext:{
            src:'-debug.txt',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('min'))
});

 
gulp.task('uglify', function() {
  return gulp.src('js/**/*.js')
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('dist'));
});

