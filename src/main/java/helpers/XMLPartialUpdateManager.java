package helpers;

import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.DocumentPatchBuilder;
import com.marklogic.client.document.DocumentPatchBuilder.Position;
import com.marklogic.client.eval.EvalResult;
import com.marklogic.client.eval.EvalResultIterator;
import com.marklogic.client.eval.ServerEvaluationCall;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.marker.DocumentPatchHandle;
import com.marklogic.client.util.EditableNamespaceContext;

import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.propisi.TClan;
import rs.gov.parlament.propisi.TDeo;
import rs.gov.parlament.propisi.TGlava;
import rs.gov.parlament.propisi.TOdeljak;
import rs.gov.parlament.propisi.TStav;
import rs.gov.parlament.propisi.TTacka;
import xml.database.DatabaseManager;
import xml.database.NSPrefixMapper;

/**
 * 
 * [PRIMER 5]
 * 
 * Primer demonstrira parcijalnu izmenu XML dokumenta upotrebom
 * DocumentPatchHandle klase MarkLogic Java API-ja. Parcijalno ažuriranje XML
 * dokumenta realizuje se zadavanjem konteksta, u vidu XPath putanje, pozicije
 * izmene, zadate relativno u odnosu na kontekst, kao i nove vrednosti čvora.
 * 
 * Nakon izvršavanja primera pokrenuti prethodni primer ili pristupiti REST
 * API-ju otvaranjem sledećeg URL-a direktno iz browser-a:
 * 
 * http://{host}:8000/v1/documents?database={database}&uri=/example/books.xml
 * 
 * alternativno, upotrebom MarkLogic-ovog Query Console-a na adresi:
 * 
 * http://{host}:8000/qconsole/
 * 
 * Za detaljan opis parametara MarkLogic-ovog klijentskog REST API-ja posetiti:
 * 
 * https://docs.marklogic.com/REST/client
 * 
 */
public class XMLPartialUpdateManager {

	private static DatabaseClient client;
	
	public static String getSadrzajAmandmanaToString(TAmandman amanObj) {
		String ret = "";
		
		Marshaller marshaller = null;
		try {
			JAXBContext contextValidation = null; 
			Object objSadrzaj = null;
			//JAXBContext.newInstance(TTacka.class);
			if (amanObj.getSadrzajAmandmana() == null) {
				//System.out.println("sadrzaj amandmana je prazan!");
				return ret;
			}
			if (amanObj.getSadrzajAmandmana().getDeo() != null) {
				contextValidation = JAXBContext.newInstance(TDeo.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getDeo();
				//System.out.println("instancirao context!");
			} else if (amanObj.getSadrzajAmandmana().getGlava() != null) {
				contextValidation = JAXBContext.newInstance(TGlava.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getGlava();
				//System.out.println("instancirao context!");
			} else if (amanObj.getSadrzajAmandmana().getOdeljak() != null) {
				contextValidation = JAXBContext.newInstance(TOdeljak.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getOdeljak();
				//System.out.println("instancirao context!");
			} else if (amanObj.getSadrzajAmandmana().getClan() != null) {
				contextValidation = JAXBContext.newInstance(TClan.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getClan();
				//System.out.println("instancirao context!");
			} else if (amanObj.getSadrzajAmandmana().getStav() != null) {
				contextValidation = JAXBContext.newInstance(TStav.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getStav();
				//System.out.println("instancirao context!");
			} else if (amanObj.getSadrzajAmandmana().getTacka() != null) {
				contextValidation = JAXBContext.newInstance(TTacka.class);
				objSadrzaj = amanObj.getSadrzajAmandmana().getTacka();
				//System.out.println("instancirao context!");
			} else {
				//System.out.println("sadrzaj amandmana je prazan!");
				return ret;
			}
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
					
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();
			
			marshaller.marshal(objSadrzaj, sw);
			//System.out.println("marsalovan!");
			String xmlString = sw.toString();
			//System.out.println(xmlString);
			//xmlString = xmlString.split("?>")[1];
			//System.out.println("@@@@@@glavni cvor sadrzaja:");
			//System.out.println(xmlString);
			if (xmlString.contains("?>")) {
				//System.out.println("@@@ postoji xml komentar, izbrisati ga!");
				String[] pom = xmlString.split("\\?>");
				xmlString = pom[1];
				//System.out.println(xmlString);
			}
			
			ret = xmlString;
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public static void executeAmandman(TAmandman amanObj) throws FileNotFoundException {
		client = DatabaseManager.getInstance().getClient();
		
		String patchParam = getSadrzajAmandmanaToString(amanObj);
		
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
        // Define a URI value for a document.
 		//****************
 		String pom1 = amanObj.getCiljniElement().split("@id=")[1];
 		pom1 = pom1.substring(1);
 		String pom2 = pom1.split("]")[0];
 		pom2 = pom2.substring(0, pom2.length()-1);
 		//****************
 		String docId = "propis" + pom2 + ".xml";	//propisID.xml
 		
 		builder.append("let $x := doc(\"" + docId + "\") ");
 		
 		if (amanObj.getAkcija().equals("brisanje")) {
			builder.append("return xdmp:node-delete($x" + amanObj.getCiljniElement() + ")");
 		} else if (amanObj.getAkcija().equals("dopuna")) {
			builder.append("return xdmp:node-insert-child($x" + amanObj.getCiljniElement() + "," + patchParam + ")");
 		} else if (amanObj.getAkcija().equals("izmena")) {
 			builder.append("return xdmp:node-replace($x" + amanObj.getCiljniElement() + "," + patchParam + ")");
 		}
 		
 		//*****************************************
		invoker.xquery(builder.toString());
	    
		response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
	    }
		//*****************************************
		
	}
	

}
