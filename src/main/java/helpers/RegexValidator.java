package helpers;

public abstract class RegexValidator {
	
	public static boolean validateValue(String regex, String value){
		return value.matches(regex);
	}
}
