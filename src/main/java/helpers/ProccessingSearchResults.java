package helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.MatchDocumentSummary;
import com.marklogic.client.query.MatchLocation;
import com.marklogic.client.query.MatchSnippet;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StringQueryDefinition;

import hello.dto.SearchResultsDto;
import xml.database.DatabaseManager;

public class ProccessingSearchResults {
	private static ProccessingSearchResults instance;
	
	private DatabaseClient client;
	
	public static final String COLLECTION_PROPISI = "/propisi";

	public static final String COLLECTION_AMANDMANI = "/amandmani";
	
	private ProccessingSearchResults() {
		client = DatabaseManager.getInstance().getClient();
	}
	
	public static ProccessingSearchResults getInstance() {
		if (instance == null) {
			instance = new ProccessingSearchResults();
		}
		return instance;
	}
	
	public List<SearchResultsDto> search(String collection, String word) {
		List<SearchResultsDto> ret = new ArrayList<SearchResultsDto>();
		// Initialize query manager
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		//String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		String criteria = word;
		queryDefinition.setCriteria(criteria);
		
		// Search within a specific collection
		queryDefinition.setCollections(collection);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("[INFO] Showing the results for: " + criteria + "\n");

		MatchDocumentSummary result;
		MatchLocation locations[];
		String text;
		
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			
			//System.out.println((i+1) + ". RESULT DETAILS: ");
			//System.out.println("Result URI: " + result.getUri());
			
			locations = result.getMatchLocations();
			//System.out.println("Document locations matched: " + locations.length + "\n");
			
			List<String> matchResults = new ArrayList<>();
			
			int matchCount = 0;
			
			for (MatchLocation location : locations) {
				//*******provera da li je pronadjen element ispod trazenog cvora
				if (location.getPath().contains("Signature"))
					continue;
				//**************************************************************
				matchCount++;
				String matchContent = "";
				matchContent += "<p>";
				for (MatchSnippet snippet : location.getSnippets()) {
					text = snippet.getText().trim();
					if (!text.equals("")) {
						matchContent += (snippet.isHighlighted()? ("<span  style='color: red; font-weight: bold; text-decoration: underline'>"
									+ text.toUpperCase() + "</span>") : text);
						matchContent += " ";
					}
				}
				matchContent += "</p>";
				//System.out.println("\n - Match location XPath: " + location.getPath());
				matchResults.add(matchContent);
				//System.out.println(matchContent);
			}
			if (matchCount > 0) {
				String idDocument = result.getUri();
				if (collection.equals(ProccessingSearchResults.COLLECTION_PROPISI)) {
					idDocument = (idDocument.split("propis")[1]).split(".xml")[0];
				} else {
					idDocument = (idDocument.split("amandman")[1]).split(".xml")[0];
				}
				SearchResultsDto matchResultDocument = new SearchResultsDto(idDocument, matchResults);
				ret.add(matchResultDocument);
			}
		}
		return ret;
	}
	
	public List<SearchResultsDto> search(String collection, String word, String node) {
		List<SearchResultsDto> ret = new ArrayList<SearchResultsDto>();
		// Initialize query manager
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		//String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		String criteria = word;
		queryDefinition.setCriteria(criteria);
		
		// Search within a specific collection
		queryDefinition.setCollections(collection);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("[INFO] Showing the results for: " + criteria + "\n");

		MatchDocumentSummary result;
		MatchLocation locations[];
		String text;
		
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			
			//System.out.println((i+1) + ". RESULT DETAILS: ");
			//System.out.println("Result URI: " + result.getUri());
			
			locations = result.getMatchLocations();
			//System.out.println("Document locations matched: " + locations.length + "\n");
			
			List<String> matchResults = new ArrayList<>();
			
			int matchCount = 0;
			
			for (MatchLocation location : locations) {
				//*******provera da li je pronadjen element ispod trazenog cvora
				if (!location.getPath().contains(node) || location.getPath().contains("Signature"))
					continue;
				//**************************************************************
				matchCount++;
				String matchContent = "";
				matchContent += "<p>";
				for (MatchSnippet snippet : location.getSnippets()) {
					text = snippet.getText().trim();
					if (!text.equals("")) {
						matchContent += (snippet.isHighlighted()? ("<span  style='color: red; font-weight: bold; text-decoration: underline'>"
									+ text.toUpperCase() + "</span>") : text);
						matchContent += " ";
					}
				}
				matchContent += "</p>";
				//System.out.println("\n - Match location XPath: " + location.getPath());
				matchResults.add(matchContent);
				//System.out.println(matchContent);
			}
			if (matchCount > 0) {
				String idDocument = result.getUri();
				if (collection.equals(ProccessingSearchResults.COLLECTION_PROPISI)) {
					idDocument = (idDocument.split("propis")[1]).split(".xml")[0];
				} else {
					idDocument = (idDocument.split("amandman")[1]).split(".xml")[0];
				}
				//System.out.println(idDocument);
				SearchResultsDto matchResultDocument = new SearchResultsDto(idDocument, matchResults);
				ret.add(matchResultDocument);
			}
		}
		return ret;
	}
	
	public static void main(String[] args) throws IOException {
		ProccessingSearchResults.getInstance().search(
				ProccessingSearchResults.COLLECTION_PROPISI, 
				"pomilovanja"/*, "glava"*/);
	}
	
}
