package helpers;

public class AmandmanDto {
	private String id;
	private String naziv;
	private String potpisnik;
	private boolean usvojen;
	private String akcija;
	
	public AmandmanDto() {
		
	}

	public AmandmanDto(String id, String naziv, String potpisnik, boolean usvojen, String akcija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.potpisnik = potpisnik;
		this.usvojen = usvojen;
		this.akcija = akcija;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPotpisnik() {
		return potpisnik;
	}

	public void setPotpisnik(String potpisnik) {
		this.potpisnik = potpisnik;
	}

	public boolean isUsvojen() {
		return usvojen;
	}

	public void setUsvojen(boolean usvojen) {
		this.usvojen = usvojen;
	}

	public String getAkcija() {
		return akcija;
	}

	public void setAkcija(String akcija) {
		this.akcija = akcija;
	}
	
}
