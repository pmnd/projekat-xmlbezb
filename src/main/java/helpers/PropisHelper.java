package helpers;

public class PropisHelper {
	private String id;
	private Boolean disabled;
	
	public PropisHelper(String id, Boolean disabled) {
		super();
		this.id = id;
		this.disabled = disabled;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	
}
