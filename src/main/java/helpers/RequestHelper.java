package helpers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public abstract class RequestHelper {
	
	public static String getEmail(HttpServletRequest req){
		String email = null;
		
		Cookie[] cookies = req.getCookies();
		for (Cookie ck : cookies) {
            if (ck.getName().equals("email")) {
            	email = ck.getValue();
            	break;
            }
        }
		try {
			email = URLDecoder.decode(email, "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return email;
	}
}
