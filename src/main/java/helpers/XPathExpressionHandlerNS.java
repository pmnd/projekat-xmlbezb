package helpers;

import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * 
 * Primer demonstrira upotrebu Xpath API-ja za potrebe izvršavanja XPath 
 * upita nad elementima DOM stabla parsiranog XML dokumenta. 
 * 
 * Namespace-aware primer.
 * 
 */
public class XPathExpressionHandlerNS {

	private static DocumentBuilderFactory documentFactory;
	
	private static TransformerFactory transformerFactory;
	
	private static XPathFactory xPathFactory;
	
	private Document document;
	
	// Prefiks/namespace URI mapiranja
	private static Map<String, String> namespaceMappings;
	
	/*
	 * Factory initialization static-block
	 */
	static {

		/* Inicijalizacija DOM fabrike */
		documentFactory = DocumentBuilderFactory.newInstance();
		documentFactory.setNamespaceAware(true);
		documentFactory.setIgnoringComments(true);
		documentFactory.setIgnoringElementContentWhitespace(true);
		
		/* Inicijalizacija Transformer fabrike */
		transformerFactory = TransformerFactory.newInstance();
		
		/* Inicijalizacija XPath fabrike */
		xPathFactory = XPathFactory.newInstance();
		
		/* Inicijalizacija namespace mapiranja */
		namespaceMappings = new HashMap<String, String>();
	}
	
	/**
	 * Evaluates the provided XPath expression.
	 * 
	 * @param expression an XPath expression to be evaluated
	 */
	public void evaluateXPath(String expression) {
		
		XPath xPath = xPathFactory.newXPath();

		/* Konfiguracija Namespace Aware XPath izraza */
		namespaceMappings.put("b", "http://www.ftn.uns.ac.rs/xpath/examples");
		namespaceMappings.put("prop", "http://www.parlament.gov.rs/propisi");
		xPath.setNamespaceContext(new NamespaceContext(namespaceMappings));
		
		XPathExpression xPathExpression;
		
		try {
			
			xPathExpression = xPath.compile(expression);

			//System.out.println("[INFO] Evaluating the expression (\"" + expression + "\")");
			
			// String singleResult = xPathExpression.evaluate(document);
			// Node singleNode = (Node) xPathExpression.evaluate(document, XPathConstants.NODE);
			// NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);

			NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
			Node node;
			
			for (int i = 0; i < nodeList.getLength(); i++) {
			
				node = nodeList.item(i);
				
				if (node.getNodeType() != Node.TEXT_NODE)
					transform(node, System.out);
			}
			
		} catch (XPathExpressionException e) {
			//System.out.println("[ERROR] Error evaluationg \"" + expression + "\" expression, line: " + e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Generates document object model for a given XML file.
	 * 
	 * @param filePath XML document file path
	 */
	public void buildDocument(String filePath) {

		try {
			
			DocumentBuilder builder = documentFactory.newDocumentBuilder();
			document = builder.parse(new File(filePath)); 

			/* Detektuju eventualne greske */
			/*if (document != null)
				System.out.println("[INFO] File parsed with no errors.");
			else
				System.out.println("[WARN] Document is null.");*/

		} catch (SAXParseException e) {
			
			/*System.out.println("[ERROR] Parsing error, line: " + e.getLineNumber() + ", uri: " + e.getSystemId());
			System.out.println("[ERROR] " + e.getMessage() );
			System.out.print("[ERROR] Embedded exception: ");*/
			
			Exception embeddedException = e;
			if (e.getException() != null)
				embeddedException = e.getException();

			// Print stack trace...
			embeddedException.printStackTrace();
			
			System.exit(0);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Serializes DOM tree to an arbitrary OutputStream.
	 *
	 * @param node a node to be serialized
	 * @param out an output stream to write the serialized 
	 * DOM representation to
	 * 
	 */
	public void transform(Node node, OutputStream out) {
		try {

			// Kreiranje instance objekta zaduzenog za serijalizaciju DOM modela
			Transformer transformer = transformerFactory.newTransformer();

			// Indentacija serijalizovanog izlaza
			transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "2");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			// Nad "source" objektom (DOM stablo) vrši se transformacija
			DOMSource source = new DOMSource(node);

			// Rezultujući stream (argument metode) 
			StreamResult result = new StreamResult(out);

			// Poziv metode koja vrši opisanu transformaciju
			transformer.transform(source, result);

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Transformation API helper method.
	 * 
	 * @param out a stream to write the serialized DOM to
	 */
	public void transform(OutputStream out) {
		transform(document, out);
	}


	/** 
	 * Convenience method for input and evaluation
	 * of user provided XPath expressions.
	 */
	private void menu() {
		
		Scanner scanner = new Scanner(System.in);
		String expression = "";
		
		while (!expression.equals("*")) {
			//System.out.println("\n[INPUT] Unesite XPath izraz (npr. \"/b:bookstore/b:book/b:title/text()\"), * za kraj: ");
			expression = scanner.next();
			
			if (!expression.equals("*"))
				evaluateXPath(expression);
		}
		
		scanner.close();
		//System.out.println("[INFO] Kraj.");
		
	}
	
	public static void main(String args[]) {

		String filePath = null;

		//System.out.println("[INFO] XPath Expression Handler (Namespace Aware)");

		if (args.length != 1) {
			filePath = "data/books_namespace.xml";
			//System.out.println("[INFO] No input file, using default \""	+ filePath + "\"");

		} else {
			filePath = args[0];
		}

		XPathExpressionHandlerNS handler = new XPathExpressionHandlerNS();

		// Kreiranje DOM stabla na osnovu XML fajla
		handler.buildDocument(filePath);

		handler.transform(System.out);

		// Unos i evaluacija XPath izraza 
		handler.menu();

	}

}
