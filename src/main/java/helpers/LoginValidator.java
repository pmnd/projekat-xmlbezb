package helpers;

import java.util.Date;

public class LoginValidator {
	
	private String ip;
	private int attemptNum;
	private Date lastTime;
	
	public LoginValidator(String ip, Date timeOfLastFailedAttempt) {
		this.ip = ip;
		this.attemptNum = 0;
		this.lastTime = timeOfLastFailedAttempt;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getAttemptNum() {
		return attemptNum;
	}

	public void setAttemptNum(int attemptNum) {
		this.attemptNum = attemptNum;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
}
