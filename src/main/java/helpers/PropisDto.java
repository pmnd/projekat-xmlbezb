package helpers;

public class PropisDto {
	private String id;
	private String naziv;
	private String potpisnik;
	private String usvojen;
	private boolean usvojenBool;
	
	public PropisDto() {
		
	}

	public PropisDto(String id, String naziv, String potpisnik, String usvojen) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.potpisnik = potpisnik;
		this.usvojen = usvojen;
		this.usvojenBool = false;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPotpisnik() {
		return potpisnik;
	}

	public void setPotpisnik(String potpisnik) {
		this.potpisnik = potpisnik;
	}

	public String getUsvojen() {
		return usvojen;
	}

	public void setUsvojen(String usvojen) {
		this.usvojen = usvojen;
	}

	public boolean isUsvojenBool() {
		return usvojenBool;
	}

	public void setUsvojenBool(boolean usvojenBool) {
		this.usvojenBool = usvojenBool;
	}


	
}
