package helpers;

public class RequestValidator {
	private String name;
	private String method;
	
	public RequestValidator(String name) {
		this.name = name;
		this.method = "GET";
	}
	
	public RequestValidator(String name, String method) {
		this.name = name;
		this.method = method;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}
