package helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.eval.EvalResult;
import com.marklogic.client.eval.EvalResultIterator;
import com.marklogic.client.eval.ServerEvaluationCall;

import hello.dto.SearchResultsMetaDto;
import xml.database.DatabaseManager;

public class SearchManagerXQuery {
	private static SearchManagerXQuery instance;
	
	private DatabaseClient client;
	
	private SearchManagerXQuery() {
		client = DatabaseManager.getInstance().getClient();
	}
	
	public static SearchManagerXQuery getInstance() {
		if (instance == null) {
			instance = new SearchManagerXQuery();
		}
		return instance;
	}
	
	public List<SearchResultsMetaDto> searchForPropisMethadata(String datumPredlaganja, String datumUsvajanja, String naziv) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
		
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		StringBuilder builder = new StringBuilder();
		
		String whereCondition = "where ( ";
		if (!datumPredlaganja.equals("")) {
			whereCondition += "$c/@datumPredlaganja = '" + datumPredlaganja + "' and ";
		}
		if (!datumUsvajanja.equals("")) {
			whereCondition += "$c/@datumUsvajanja = '" + datumUsvajanja + "' and ";
		}
		whereCondition += "contains($c/prop:naziv,'" + naziv +"') ) ";
		
		builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		 builder.append("for $c in /prop:propis ");
		 builder.append(whereCondition);
		 //builder.append("return data($c/@id)");
		 //concat($x/Name,' ',$x/LastName)
		 builder.append("return data(concat($c/@id,'@',$c/prop:naziv))");
		 
		 //System.out.println(builder.toString());
		 
		 
		 //*****************************************
		 invoker.xquery(builder.toString());
	     response = invoker.eval();
	     
	     for (EvalResult result : response) {
	    	 //System.out.println(result.getString());
			String x = result.getString();
			SearchResultsMetaDto retPropis = new SearchResultsMetaDto(x.split("@")[0], x.split("@")[1]);
			ret.add(retPropis);
	     }
		 //*****************************************
		
	     //System.out.println("zavrsena pretraga metapodataka");
	     
		return ret;
	}
	
	public List<SearchResultsMetaDto> searchForAmandmanMethadata(String datumPredlaganja, String naziv) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
		
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		StringBuilder builder = new StringBuilder();
		
		String whereCondition = "where ( ";
		if (!datumPredlaganja.equals("")) {
			whereCondition += "$c/@datumPredlaganja = '" + datumPredlaganja + "' and ";
		}
		
		whereCondition += "contains($c/aman:naziv,'" + naziv +"') ) ";
		
		builder.append("declare namespace aman=\"http://www.parlament.gov.rs/amandmani\"; ");
		
		 builder.append("for $c in /aman:amandman ");
		 builder.append(whereCondition);
		 //builder.append("return data($c/@id)");
		 //concat($x/Name,' ',$x/LastName)
		 builder.append("return data(concat($c/@id,'@',$c/aman:naziv))");
		 
		 //System.out.println(builder.toString());
		 
		 
		 //*****************************************
		 invoker.xquery(builder.toString());
	     response = invoker.eval();
	     
	     for (EvalResult result : response) {
	    	 //System.out.println(result.getString());
			String x = result.getString();
			SearchResultsMetaDto retAman = new SearchResultsMetaDto(x.split("@")[0], x.split("@")[1]);
			ret.add(retAman);
	     }
		 //*****************************************
		
	     //System.out.println("zavrsena pretraga metapodataka");
	     
		return ret;
	}
	
	public List<PropisDto> findAllPropisDto() {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<PropisDto> ret = new ArrayList<PropisDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		builder.append("for $c in /prop:propis ");
		builder.append("return data(concat($c/@id,'@',$c/prop:naziv,'@',$c/prop:potpisnik,'@',$c/prop:usvojen))");
		 
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			PropisDto retPropis = new PropisDto(x.split("@")[0], x.split("@")[1], x.split("@")[2], x.split("@")[3]);
			ret.add(retPropis);
	    }
		//*****************************************
        
        return ret;
	}
	
	public List<AmandmanDto> findAllAmanDto() {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<AmandmanDto> ret = new ArrayList<AmandmanDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace aman=\"http://www.parlament.gov.rs/amandmani\"; ");
		
		builder.append("for $c in /aman:amandman ");
		builder.append("return data(concat($c/@id,'_@_',$c/aman:naziv,'_@_',$c/aman:potpisnik,'_@_',$c/aman:usvojen,'_@_',$c/aman:ciljniElement,'_@_',$c/aman:akcija))");
		
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			boolean usvojenParam = false;
	    	if ((x.split("_@_")[3]).equals("true")) {
	    		usvojenParam = true;
	    	}
			
	    	AmandmanDto retAman = new AmandmanDto(x.split("_@_")[0], x.split("_@_")[1], x.split("_@_")[2], usvojenParam, x.split("_@_")[5]);
	    	ret.add(retAman);
	    }
		//*****************************************
	    //System.out.println("zavrsena pretraga");
	    
        return ret;
	}
	
	public List<PropisDto> findPropisDtoCollection(String collection) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<PropisDto> ret = new ArrayList<PropisDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		builder.append("for $c in /prop:propis ");
		builder.append("return data(concat($c/@id,'@',$c/prop:naziv,'@',$c/prop:potpisnik,'@',$c/prop:usvojen))");
		 
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			if ((x.split("@")[3]).equals(collection)) { 
				PropisDto retPropis = new PropisDto(x.split("@")[0], x.split("@")[1], x.split("@")[2], x.split("@")[3]);
				ret.add(retPropis);
			}
	    }
		//*****************************************
        
        return ret;
	}
	
	public List<AmandmanDto> findAmandmaniOdredjenogPropisaDto(String idPropisa) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<AmandmanDto> ret = new ArrayList<AmandmanDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace aman=\"http://www.parlament.gov.rs/amandmani\"; ");
		
		builder.append("for $c in /aman:amandman ");
		builder.append("return data(concat($c/@id,'_@_',$c/aman:naziv,'_@_',$c/aman:potpisnik,'_@_',$c/aman:usvojen,'_@_',$c/aman:ciljniElement,'_@_',$c/aman:akcija))");
		 
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			boolean usvojenParam = false;
	    	if ((x.split("_@_")[3]).equals("true")) {
	    		usvojenParam = true;
	    	}
			String ciljniElement = x.split("_@_")[4];
			//****************
			String pom1 = ciljniElement.split("@id=")[1];
			pom1 = pom1.substring(1);
			String pom2 = pom1.split("]")[0];
			pom2 = pom2.substring(0, pom2.length()-1);
			//****************
			if (pom2.equals(idPropisa)) {
				AmandmanDto retAman = new AmandmanDto(x.split("_@_")[0], x.split("_@_")[1], x.split("_@_")[2], usvojenParam, x.split("_@_")[5]);
				//AmandmanDto retAman = new AmandmanDto(x.split("@")[0], x.split("@")[1], x.split("@")[2]);
				ret.add(retAman);
			}
	    }
		//*****************************************
	    //System.out.println("zavrsena pretraga!");
        
        return ret;
	}
	
	public static void main(String[] args) throws IOException {
		//SearchManagerXQuery.getInstance().searchForPropisMethadata("2006-05-04", "", "");
		//SearchManagerXQuery.getInstance().searchForAmandmanMethadata("2006-05-04", "");
		//SearchManagerXQuery.getInstance().findAllPropisDto();
		SearchManagerXQuery.getInstance().findAllAmanDto();
		//SearchManagerXQuery.getInstance().findAmandmaniOdredjenogPropisaDto("1");
	}
	
}
