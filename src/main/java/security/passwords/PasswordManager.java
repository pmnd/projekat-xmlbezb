package security.passwords;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public abstract class PasswordManager {
	
	public static String generateSalt(){
		// vrlo je vazno pri svim kriptografskim operacijama koristiti SecureRandom generator 
		// pseudo slucajnih brojeva 
		SecureRandom random = null;
		byte[] salt = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
			salt = new byte[8];
			random.nextBytes(salt);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return base64Encode(salt);
	}
	
	public static String generateHashedPassword(String password, byte[] salt){

		  String algorithm = "PBKDF2WithHmacSHA1";
		  int derivedKeyLength = 160;
		  
		  // broj iteracija se moze podesavati i na taj nacin povecati ili smanjiti slozenost hashiranja
		  // veci broj znaci vise iteracija hash-iranja 
		  int iterations = 1000;
		  KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);
		  SecretKeyFactory f;
		  
		  try {
				f = SecretKeyFactory.getInstance(algorithm);
				return base64Encode(f.generateSecret(spec).getEncoded()); // vratimo hashiranu vrednost lozinke
		  } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
		  }
		  
		return null;  
	}
	
	public static boolean authenticateUser(String attemptedPassword, String storedPassword, String salt){
		  
		  byte[] hashedAttemptedPassword = null;
		  try {
			  //unesena lozinka se hashuje sa odgovarajucim salt-om  
			  hashedAttemptedPassword = base64Decode(generateHashedPassword(attemptedPassword, base64Decode(salt)));
			  return Arrays.equals(base64Decode(storedPassword), hashedAttemptedPassword);
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
		  return false;
	}
	
	public static String base64Encode(byte[] data){
		 String result = null;
		 BASE64Encoder encoder = new BASE64Encoder();
		 result = encoder.encode(data);
		 return result;
	 }
	
	public static byte[] base64Decode(String base64Data) throws IOException{
		 BASE64Decoder decoder = new BASE64Decoder();
		 return decoder.decodeBuffer(base64Data);
	 }
	
}