<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:aman="http://www.parlament.gov.rs/amandmani"
	xmlns:prop="http://www.parlament.gov.rs/propisi"  version="2.0">

    <xsl:template match="/">
       <div>
                <style type="text/css">
                    
                    p.nazivAmandmana { text-align:left; font-size: 12px;margin: 0cm 4cm 0cm 4cm; }
					p.opisAmandmana { text-align:left; font-size: 12px; margin: 0cm 4cm 0cm 4cm;}
					p.ciljniElementAmandmana { text-align:left; font-size: 12px;margin: 0cm 4cm 0cm 4cm; }
					p.akcijaAmandmana { text-align:left; font-size: 12px;margin: 0cm 4cm 0cm 4cm; }
					body { font-family: sans-serif; }
                    p { text-indent: 30px ; text-align:center }
					h1, h2, h3, h4, h5, h6 { text-indent: 30px ; text-align:center }
					p.tekstTacke {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
                    p.tekstStava {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstGlave {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstOdeljka {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstClana {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm; font-weight: normal;}
					p.opisTacke {text-indent: 100px; text-align: justify; margin: 0cm 8cm 0cm 8cm;}
					p.nazivStava {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstStavaClan {text-indent: 90px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
					p.tekstTackeStav {text-indent: 90px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
					p.tekstTackeGlava {text-indent: 90px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
					p.tekstTackeOdeljak {text-indent: 90px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
                </style>
     
				<p class="nazivAmandmana">Naziv:<b><xsl:value-of select="aman:amandman/aman:naziv" /></b></p>
				<p class="opisAmandmana">Opis:<b><xsl:value-of select="aman:amandman/prop:preambula/prop:opis" /></b></p>
				<p class="ciljniElementAmandmana">Ciljni element:<b><xsl:value-of select="aman:amandman/aman:ciljniElement" /></b></p>
				<p class="akcijaAmandmana">Akcija:<b><xsl:value-of select="aman:amandman/aman:akcija" /></b></p>
				
				<h2 class="nazivAman"><xsl:value-of select="aman:amandman/aman:naziv" /></h2>
				<p class="opisAman"><xsl:value-of select="aman:amandman/prop:preambula/prop:opis" /></p>
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:clan">
					<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /></h4>
					<h4 class="opisClana"><xsl:value-of select="prop:opis" /></h4>
					<xsl:if test="prop:tekstClana != ''">
							<xsl:if test="not(prop:tekstClana/prop:refer)">
								<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
							</xsl:if>
                            <xsl:for-each select="prop:tekstClana/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajClana/prop:stav">
						<p class="nazivStava"><xsl:value-of select="prop:naziv" /></p>
						<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
						<xsl:if test="prop:tekstStava != ''">
								<xsl:if test="not(prop:tekstStava/prop:refer)">
									<p class="tekstStavaClan"><xsl:value-of select="prop:tekstStava" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstStava/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajStava/prop:tacka">
							<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
							<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
							<xsl:if test="prop:tekstTacke != ''">
								<xsl:if test="not(prop:tekstTacke/prop:refer)">
									<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstTacke/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:for-each>
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:deo">
					<h3 class="nazivDela"><xsl:value-of select="prop:naziv" /></h3>
					<h3 class="opisDela"><xsl:value-of select="prop:opis" /></h3>
					<xsl:if test="prop:tekstDela != ''">
						<xsl:if test="not(prop:tekstDela/prop:refer)">
							<p class="tekstDela"><xsl:value-of select="prop:tekstDela" /> </p>
						</xsl:if>
                        <xsl:for-each select="prop:tekstDela/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
					<br />
					<xsl:for-each select="prop:sadrzajDela/prop:glava">
						<h4 class="nazivGlave"><xsl:value-of select="prop:naziv" /></h4>
						<h4 class="opisGlave"><xsl:value-of select="prop:opis" /></h4>
						<xsl:if test="prop:tekstGlave != ''">
							<xsl:if test="not(prop:tekstGlave/prop:refer)">
								<p class="tekstGlave"><xsl:value-of select="prop:tekstGlave" /> </p>
							</xsl:if>
                            <xsl:for-each select="prop:tekstGlave/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
							<h4 class="nazivOdeljka"><xsl:value-of select="prop:naziv" /></h4>
							<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /></h4>
							<xsl:if test="prop:tekstOdeljka != ''">
								<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
									<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
								</xsl:if>
                                 <xsl:for-each select="prop:tekstOdeljka/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
							
							<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
								<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /></h4>
								<h4 class="opisClana"><xsl:value-of select="prop:opis" /></h4>
								<xsl:if test="prop:tekstClana != ''">
									<xsl:if test="not(prop:tekstClana/prop:refer)">
										<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
									</xsl:if>
									<xsl:for-each select="prop:tekstClana/prop:refer">
										<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<p class="nazivStava"><xsl:value-of select="prop:naziv" /></p>
										<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
											<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
							
							<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
								<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
								<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
								<xsl:if test="prop:tekstOdeljka != ''">
									<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
										<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
									</xsl:if>
									<xsl:for-each select="prop:tekstOdeljka/prop:refer">
										<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
									</xsl:for-each>
								</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
									<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<p class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b></p>
										<p class="opisClana"><b><xsl:value-of select="prop:opis" /></b></p>
										<xsl:if test="prop:tekstClana != ''">
											<xsl:if test="not(prop:tekstClana/prop:refer)">
												<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstClana/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<p class="nazivStava"><b><xsl:value-of select="prop:naziv" /></b></p>
										<p class="opisStava"><b><xsl:value-of select="prop:opis" /></b></p>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<p class="nazivTacke"><b><xsl:value-of select="prop:naziv" /></b></p>
											<p class="opisTacke"><b><xsl:value-of select="prop:opis" /></b></p>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:for-each>
						
						<xsl:for-each select="prop:sadrzajGlave/prop:clan">
							<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /></h4>
							<h4 class="opisClana"><xsl:value-of select="prop:opis" /></h4>
							<xsl:if test="prop:tekstClana != ''">
								<xsl:if test="not(prop:tekstClana/prop:refer)">
									<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
								</xsl:if>
								<xsl:for-each select="prop:tekstClana/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
							<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<p class="nazivStava"><xsl:value-of select="prop:naziv" /></p>
										<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
											<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<p class="tekstTackeStav"><xsl:value-of select="prop:tekstTacke" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
							</xsl:if>
						
						
						
						</xsl:for-each>
					</xsl:for-each>
				</xsl:for-each>
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:glava">
					<h3 class="nazivGlave"><xsl:value-of select="prop:naziv" /></h3>
					<h3 class="opisGlave"><xsl:value-of select="prop:opis" /></h3>
					<xsl:if test="prop:tekstGlave != ''">
						<xsl:if test="not(prop:tekstGlave/prop:refer)">
								<p class="tekstGlave"><xsl:value-of select="prop:tekstGlave" /> </p>
						</xsl:if>
                         <xsl:for-each select="prop:tekstGlave/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						 </xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
						<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
						<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
						<xsl:if test="prop:tekstOdeljka != ''">
							<xsl:for-each select="prop:tekstOdeljka/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
							<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
							<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
							<xsl:if test="prop:tekstOdeljka != ''">
								<xsl:for-each select="prop:tekstOdeljka/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
							<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
								<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
								<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
								<xsl:if test="prop:tekstOdeljka != ''">
									<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka"/></p>
								</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<p class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b></p>
									<p class="opisOdeljka"><b><xsl:value-of select="prop:opis" /></b></p>
									<xsl:if test="prop:tekstOdeljka != ''">
										<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka"/></p>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<p class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b></p>
										<p class="opisClana"><b><xsl:value-of select="prop:opis" /></b></p>
										<xsl:if test="prop:tekstClana != ''">
											<p class="tekstClana"><xsl:value-of select="prop:tekstClana"/></p>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
											<p class="sadrzajClana"><xsl:value-of select="prop:sadrzajClana"/></p>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<p class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b></p>
										<p class="opisClana"><b><xsl:value-of select="prop:opis" /></b></p>
										<xsl:if test="prop:tekstClana != ''">
											<p class="tekstClana"><xsl:value-of select="prop:tekstClana"/></p>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
											<p class="sadrzajClana"><xsl:value-of select="prop:sadrzajClana"/></p>
										</xsl:if>
								</xsl:for-each>
								
							</xsl:for-each>
						</xsl:for-each>
						<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
							<p class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b></p>
							<p class="opisClana"><xsl:value-of select="prop:opis" /></p>
							<xsl:if test="prop:tekstClana != ''">
								<xsl:for-each select="prop:tekstClana/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
					<xsl:for-each select="prop:sadrzajClana/prop:stav">
						<p class="nazivStava"><b><xsl:value-of select="prop:naziv" /></b></p>
						<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
						<xsl:if test="prop:tekstStava != ''">
                                <xsl:for-each select="prop:tekstStava/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajStava/prop:tacka">
							<p class="nazivTacke"><b><xsl:value-of select="prop:naziv" /></b></p>
							<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
							<xsl:if test="prop:tekstTacke != ''">
                                <xsl:for-each select="prop:tekstTacke/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
						</xsl:for-each>
					</xsl:for-each>
					
					<xsl:for-each select="prop:sadrzajGlave/prop:clan">
					<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /></h4>
					<h4 class="opisClana"><xsl:value-of select="prop:opis" /></h4>
					<xsl:if test="prop:tekstClana != ''">
							<xsl:if test="not(prop:tekstClana/prop:refer)">
								<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
							</xsl:if>
                            <xsl:for-each select="prop:tekstClana/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajClana/prop:stav">
						<p class="nazivStava"><xsl:value-of select="prop:naziv" /></p>
						<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
						<xsl:if test="prop:tekstStava != ''">
								<xsl:if test="not(prop:tekstStava/prop:refer)">
									<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstStava/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajStava/prop:tacka">
							<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
							<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
							<xsl:if test="prop:tekstTacke != ''">
								<xsl:if test="not(prop:tekstTacke/prop:refer)">
									<p class="tekstTackeGlava"><xsl:value-of select="prop:tekstTacke" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstTacke/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:for-each>
					
				</xsl:for-each>
				
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:tacka">
					<h4 class="nazivTacke"><xsl:value-of select="prop:naziv" /></h4>
					<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /></h4>
					<xsl:if test="prop:tekstTacke != ''">
						<xsl:if test="not(prop:tekstTacke/prop:refer)">
								<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
						</xsl:if>
						<xsl:for-each select="prop:tekstTacke/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
				</xsl:for-each>
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:stav">
					<h4 class="nazivStava"><xsl:value-of select="prop:naziv" /></h4>
					<h4 class="opisStava"><xsl:value-of select="prop:opis" /></h4>
					<xsl:if test="prop:tekstStava != ''">
						<xsl:if test="not(prop:tekstStava/prop:refer)">
								<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
						</xsl:if>
						<xsl:for-each select="prop:tekstStava/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajStava/prop:tacka">
						<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
						<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
						<xsl:if test="prop:tekstTacke != ''">
							<xsl:if test="not(prop:tekstTacke/prop:refer)">
								<p class="tekstTackeStav"><xsl:value-of select="prop:tekstTacke" /> </p>
							</xsl:if>
							<xsl:for-each select="prop:tekstTacke/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
				
				<xsl:for-each select="aman:amandman/aman:sadrzajAmandmana/prop:odeljak">
					<h2 class="nazivOdeljka"><xsl:value-of select="prop:naziv" /></h2>
					<h2 class="opisOdeljka"><xsl:value-of select="prop:opis" /></h2>
					<xsl:if test="prop:tekstOdeljka != ''">
						<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
								<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
						</xsl:if>
						<xsl:for-each select="prop:tekstOdeljka/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
					<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /></h4>
					<h4 class="opisClana"><xsl:value-of select="prop:opis" /></h4>
					<xsl:if test="prop:tekstClana != ''">
							<xsl:if test="not(prop:tekstClana/prop:refer)">
								<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
							</xsl:if>
                            <xsl:for-each select="prop:tekstClana/prop:refer">
								<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
							</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajClana/prop:stav">
						<p class="nazivStava"><xsl:value-of select="prop:naziv" /></p>
						<p class="opisStava"><xsl:value-of select="prop:opis" /></p>
						<xsl:if test="prop:tekstStava != ''">
								<xsl:if test="not(prop:tekstStava/prop:refer)">
									<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstStava/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajStava/prop:tacka">
							<p class="nazivTacke"><xsl:value-of select="prop:naziv" /></p>
							<p class="opisTacke"><xsl:value-of select="prop:opis" /></p>
							<xsl:if test="prop:tekstTacke != ''">
								<xsl:if test="not(prop:tekstTacke/prop:refer)">
									<p class="tekstTackeOdeljak"><xsl:value-of select="prop:tekstTacke" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstTacke/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
					</xsl:for-each>
					<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<h4 class="nazivOdeljka"><xsl:value-of select="prop:naziv" /> </h4>
									<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /> </h4>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<h5 class="nazivClana"><xsl:value-of select="prop:naziv" /> </h5>
										<h5 class="opisClana"><xsl:value-of select="prop:opis" /> </h5>
										<xsl:if test="prop:tekstClana != ''">
											<xsl:if test="not(prop:tekstClana/prop:refer)">
												<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstClana/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
											<xsl:for-each select="prop:sadrzajClana/prop:stav">
												<p class="nazivStava"><xsl:value-of select="prop:naziv"/></p>
												<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
												<xsl:if test="prop:tekstStava != ''">
													<xsl:if test="not(prop:tekstStava/prop:refer)">
														<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
													</xsl:if>
													<xsl:for-each select="prop:tekstStava/prop:refer">
														<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
													</xsl:for-each>
												</xsl:if>
												<xsl:for-each select="prop:sadrzajStava/prop:tacka">
													<p class="nazivTacke"><xsl:value-of select="prop:naziv"/></p>
													<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
													<xsl:if test="prop:tekstTacke != ''">
														<xsl:if test="not(prop:tekstTacke/prop:refer)">
															<p class="tekstTackeOdeljak"><xsl:value-of select="prop:tekstTacke" /> </p>
														</xsl:if>
														<xsl:for-each select="prop:tekstTacke/prop:refer">
															<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
					</xsl:for-each>
				</xsl:for-each>
		</div>	
    </xsl:template>
</xsl:stylesheet>