<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:prop="http://www.parlament.gov.rs/propisi"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"	version="2.0">
	
    <xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
                <fo:simple-page-master master-name="bookstore-page">
                    <fo:region-body margin="1in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
			
			<fo:page-sequence master-reference="bookstore-page">
				<fo:flow flow-name="xsl-region-body">
				<fo:block font-family="sans-serif" font-size="18px" font-weight="bold" text-align="center" padding="10px">
                        <fo:inline font-weight="bold"><xsl:value-of select="prop:propis/prop:naziv" /></fo:inline>
				</fo:block>
				<fo:block font-family="sans-serif" font-size="14px"  text-align="center" padding="10px">
                        <fo:inline><xsl:value-of select="prop:propis/prop:preambula/prop:opis" /></fo:inline>
				</fo:block>
				
				<xsl:if test="prop:propis/prop:tekstPropisa != ''">
					<xsl:if test="not(prop:propis/prop:tekstPropisa/prop:refer)">
					<fo:block font-family="sans-serif" font-size="14px"  text-align="justify" padding="5px">
							<fo:inline><xsl:value-of select="prop:propis/prop:tekstPropisa" /></fo:inline>
					</fo:block>
					</xsl:if>
                    <xsl:for-each select="prop:propis/prop:tekstPropisa/prop:refer">
						<fo:block font-family="sans-serif" font-size="14px"  text-align="center" padding="5px">
							<fo:inline> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
						</fo:block>
					</xsl:for-each>
				</xsl:if>
				
				<xsl:for-each select="prop:propis/prop:sadrzajPropisa/prop:deo">
					<fo:block font-family="sans-serif" font-size="16px" font-weight="bold" text-align="center" padding="10px">
						<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
					</fo:block>	
					<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
						<fo:inline ><xsl:value-of select="prop:opis" /></fo:inline>
					</fo:block>	
					
					<xsl:if test="prop:tekstDela != ''">
						<xsl:if test="not(prop:tekstDela/prop:refer)">
							<fo:block font-family="sans-serif" font-size="14px" text-align="justify">
								<fo:inline ><xsl:value-of select="prop:tekstDela" /></fo:inline>
							</fo:block>
						</xsl:if>
                        <xsl:for-each select="prop:tekstDela/prop:refer">
							<fo:block font-family="sans-serif" font-size="14px" text-align="center">
								<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
							</fo:block>
						</xsl:for-each>
					</xsl:if>
					<fo:block></fo:block>
					<xsl:for-each select="prop:sadrzajDela/prop:glava">
						<fo:block font-family="sans-serif" font-size="16px" font-weight="bold" text-align="center" padding="10px">
							<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
						</fo:block>
						<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
							<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
						</fo:block>
						<xsl:if test="prop:tekstGlave != ''">
								<xsl:if test="not(prop:tekstGlave/prop:refer)">
									<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
										<fo:inline ><xsl:value-of select="prop:tekstGlave" /></fo:inline>
									</fo:block>
								</xsl:if>
                                <xsl:for-each select="prop:tekstGlave/prop:refer">
									<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
										<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
									</fo:block>
								</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="prop:sadrzajGlave/prop:clan">
							<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
								<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
							</fo:block>
							<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
								<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
							</fo:block>
							<xsl:if test="prop:tekstClana != ''">
									<xsl:if test="not(prop:tekstClana/prop:refer)">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
											<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
										</fo:block>
									</xsl:if>
									<xsl:for-each select="prop:tekstClana/prop:refer">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
											<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
										</fo:block>
									</xsl:for-each>
							</xsl:if>
							<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
						
						
						
						<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
								<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
									<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
								</fo:block>
								<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
									<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
								</fo:block>
								<xsl:if test="prop:tekstOdeljka != ''">
									<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
											<fo:inline ><xsl:value-of select="prop:tekstOdeljka" /></fo:inline>
										</fo:block>
									</xsl:if>
									<xsl:for-each select="prop:tekstOdeljka/prop:refer">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
											<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
										</fo:block>
									</xsl:for-each>
								</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
								
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstOdeljka" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
						</xsl:for-each>
					<fo:block padding="30px"></fo:block>
					</xsl:for-each>	
				</xsl:for-each>		
				
				
				
				
				
				
				
				<xsl:for-each select="prop:propis/prop:sadrzajPropisa/prop:glava">
					<fo:block font-family="sans-serif" font-size="16px" font-weight="bold" text-align="center" padding="10px">
						<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
					</fo:block>
					<fo:block font-family="sans-serif" font-size="14px"  text-align="center" padding="10px">
						<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
					</fo:block>
					<xsl:if test="prop:tekstGlave != ''">
								<xsl:if test="not(prop:tekstGlave/prop:refer)">
									<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
										<fo:inline ><xsl:value-of select="prop:tekstGlave" /></fo:inline>
									</fo:block>
								</xsl:if>
                                <xsl:for-each select="prop:tekstGlave/prop:refer">
									<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
										<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
									</fo:block>
								</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajGlave/prop:clan">
							<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
								<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
							</fo:block>
							<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
								<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
							</fo:block>
							<xsl:if test="prop:tekstClana != ''">
									<xsl:if test="not(prop:tekstClana/prop:refer)">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
											<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
										</fo:block>
									</xsl:if>
									<xsl:for-each select="prop:tekstClana/prop:refer">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
											<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
										</fo:block>
									</xsl:for-each>
							</xsl:if>
							<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
							</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstOdeljka" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstOdeljka" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:naziv" /></fo:inline>
									</fo:block>
									<fo:block font-family="sans-serif" font-size="14px" font-weight="bold" text-align="center" padding="10px">
										<fo:inline font-weight="bold"><xsl:value-of select="prop:opis" /></fo:inline>
									</fo:block>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
												<fo:inline ><xsl:value-of select="prop:tekstClana" /></fo:inline>
											</fo:block>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
												<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
											</fo:block>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center" >
											<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
											<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
										</fo:block>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" padding="5px">
													<fo:inline ><xsl:value-of select="prop:tekstStava" /></fo:inline>
												</fo:block>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<fo:block font-family="sans-serif" font-size="12px"  text-align="center" padding="5px">
													<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
												</fo:block>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
												<fo:inline ><xsl:value-of select="prop:naziv"/></fo:inline>
											</fo:block>
											<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
												<fo:inline ><xsl:value-of select="prop:opis"/></fo:inline>
											</fo:block>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="justify" text-indent="50px">
														<fo:inline ><xsl:value-of select="prop:tekstTacke" /></fo:inline>
													</fo:block>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<fo:block font-family="sans-serif" font-size="12px"  text-align="center">
														<fo:inline ><a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></fo:inline>
													</fo:block>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
				</xsl:for-each>
				
				
				
				
				
				
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
    </xsl:template>
</xsl:stylesheet>
				
				