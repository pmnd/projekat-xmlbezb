package hello.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import hello.dto.ClearDto;
import hello.dto.SearchParamDto;
import hello.dto.SearchParamMetaDto;
import hello.dto.SearchResultsDto;
import hello.dto.SearchResultsMetaDto;
import hello.service.AmandmanService;
import helpers.AmandmanDto;
import helpers.ProccessingSearchResults;
import helpers.RequestHelper;
import helpers.SearchManagerXQuery;
import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.propisi.TPropis;
import web.html.XSLFOTransformer;
import xml.database.SchemaValidationManager;

@RestController
@RequestMapping(value = "/rest/amandman")
public class AmandmanRest {
	@Autowired
	AmandmanService amandmanService;

	final static Logger logger = LoggerFactory.getLogger(AmandmanRest.class);
	@Autowired 
	private HttpServletRequest request;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<AmandmanDto> getAll() {
		return SearchManagerXQuery.getInstance().findAllAmanDto();
	}
	
	//staviti media type da je xml!!!!!!!
	@RequestMapping(method = RequestMethod.POST)
	public String insert(@RequestBody ClearDto clearObj) {
		String token = request.getHeader("Authorization");
		
		JAXBContext jaxbContext;
		TAmandman noviAmandman = null;
		try {
			jaxbContext = JAXBContext.newInstance(TPropis.class);
			//Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Unmarshaller unmarshaller = SchemaValidationManager.getAmandmanUnmarshaller();
			//System.out.println("instancirn unmarsall: " + unmarshaller);
			StringReader reader = new StringReader(clearObj.getParam());
			//System.out.println("reader: " + reader);
			noviAmandman = (TAmandman) (TAmandman)JAXBIntrospector.getValue(unmarshaller.unmarshal(reader));
		} catch (JAXBException e) {

			logger.error(e.getMessage());
		}
		
		/*if (noviAmandman == null) {
			System.out.println("objekat je null");
		} else {
			System.out.println("nziv objekta: " + noviAmandman.getNaziv());
		}*/
		noviAmandman.setId(Long.toString(System.currentTimeMillis()));
		noviAmandman.setPotpisnik(token);
		//amandmanService.create(noviAmandman);
		amandmanService.createAuthorizedAmandman(noviAmandman, RequestHelper.getEmail(request));
		
		return "";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public TAmandman getOne(@PathVariable("id") String id) {
		return amandmanService.findById(id);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean remove(@PathVariable("id") String id) {
		return amandmanService.delete(id);
	}
	
	//staviti media type da je xml!!!!!!!
	@RequestMapping(method = RequestMethod.PUT)
	public Boolean update(@RequestBody TAmandman sed) {
		return amandmanService.update(sed);
	}
	
	@RequestMapping(value = "/updateList", method = RequestMethod.PUT)
	public Boolean updateList(@RequestBody ArrayList<AmandmanDto> amandmani) {
		boolean retVal = true;
		
		for(AmandmanDto aman:amandmani){
			TAmandman amanObj = amandmanService.findById(aman.getId());
			if (amanObj == null)
				continue;
			amanObj.setUsvojen(aman.isUsvojen());
			boolean flag = amandmanService.update(amanObj);
			if(!flag)
				retVal = flag;
		}
		return retVal;
	}
	
	@RequestMapping(value = "/propis/{id}", method = RequestMethod.GET)
	public List<AmandmanDto> getAmandmaniOdredjenogPropisa(@PathVariable("id") String id) {
		return SearchManagerXQuery.getInstance().findAmandmaniOdredjenogPropisaDto(id);
	}
	
	
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<SearchResultsDto> getSearchWord(@RequestBody SearchParamDto param) {
		List<SearchResultsDto> ret = new ArrayList<SearchResultsDto>();
		
		if (param.getElement().equals("")) {
			ret = ProccessingSearchResults.getInstance().search(
					ProccessingSearchResults.COLLECTION_AMANDMANI,
					param.getValue());
		} else {
			ret = ProccessingSearchResults.getInstance().search(
					ProccessingSearchResults.COLLECTION_AMANDMANI,
					param.getValue(), param.getElement());
		}
		
		return ret;
	}
	
	@RequestMapping(value = "/search/meta", method = RequestMethod.POST)
	public List<SearchResultsMetaDto> getSearchMeta(@RequestBody SearchParamMetaDto param) {
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		ret = SearchManagerXQuery.getInstance().searchForAmandmanMethadata(
				param.getDatumPredlaganja(), 
				param.getNaslov());
		
		return ret;
	}
	
	@RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET)
	public void getPDFFile(
	    @PathVariable("id") String id, 
	    HttpServletResponse response) {
	    try {
	    	
	    	try {
				XSLFOTransformer xslfot = new XSLFOTransformer();
				try {
					xslfot.generatePDFAmandman(amandmanService.createAmanFileWithId(id));
				} catch (Exception e) {
					// TODO Auto-generated catch block

					logger.error(e.getMessage());
				}
			} catch (SAXException e) {
				// TODO Auto-generated catch block

				logger.error(e.getMessage());
			}
	    	
		      // get your file as InputStream
		      InputStream is = new FileInputStream(new File("tempFiles/amandman"+id+".pdf"));
		      // copy it to response's OutputStream
		      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
		      response.flushBuffer();
	    } catch (IOException ex) {

			logger.error(ex.getMessage());
	    }

	}
	
}
