package hello.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.service.SednicaService;
import rs.gov.parlament.sednice.TSednica;

@RestController
@RequestMapping(value = "/rest/sednica")
public class SednicaRest {

	@Autowired
	SednicaService sednicaService;
	

	final static Logger logger = LoggerFactory.getLogger(SednicaRest.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TSednica> getAll() {
		return sednicaService.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String insert(@RequestBody TSednica sednica) {
		String ret = sednicaService.create(sednica);
		
		return ret;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public TSednica getOne(@PathVariable("id") String id) {
		return sednicaService.findById(id);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean remove(@PathVariable("id") String id) {
		return sednicaService.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Boolean update(@RequestBody TSednica sed) {
		return sednicaService.update(sed);
	}
	
	@RequestMapping(value = "/sednicaZapoceta", method = RequestMethod.POST)
	public void sednicaZapoceta(@RequestBody boolean flag) {
		sednicaService.setSednicaZapoceta(flag);
	}
	
	@RequestMapping(value = "/sednicaZapoceta", method = RequestMethod.GET)
	public Boolean sednicaZapoceta() {
		return sednicaService.isSednicaZapoceta();
	}
	
}
