package hello.web;

import java.io.FileNotFoundException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import hello.service.AmandmanService;
import helpers.XMLPartialUpdateManager;
import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.propisi.TClan;
import rs.gov.parlament.propisi.TTacka;
import web.html.XSLFOTransformer;
import xml.database.NSPrefixMapper;

@RestController
@RequestMapping(value = "/rest/html/amandman")
public class AmandmanHTMLRest {

	@Autowired
	AmandmanService amandmanService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getOne(@PathVariable("id") String id) {
		Gson gson = new Gson();
		return gson.toJson(
				XSLFOTransformer.generateHTMLAmandman(amandmanService.getFileById(id))
		);
	}
	
}
