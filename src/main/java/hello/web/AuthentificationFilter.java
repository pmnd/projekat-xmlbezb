package hello.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hello.service.KorisnikServiceImpl;
import hello.utils.AuthorizationManager;
import helpers.LoginValidator;
import helpers.RequestValidator;

@Component
public class AuthentificationFilter implements Filter{

	private static final String HOST = "https://localhost:8080";
	private static final String ULOGA_PREDSEDNIK = "predsednik";
	private static final String ULOGA_ODBORNIK = "odbornik";
	private static final String ULOGA_GRADJANIN = "gradjanin";
	
	private Map<String,ArrayList<RequestValidator>> map;
	private ArrayList<String> slobodneRute;
	private HashMap<String, LoginValidator> requestInfoMap = new HashMap<String,LoginValidator>();
	
	
	private final long ONE_MINUTE_IN_MILLIS = 60000;
	private final int LOGGING_BAN_MINUTES = 5;
	private final int MAX_REQUESTS = 100;
	private final String[] HEADERS_TO_TRY = { 
		    "X-Forwarded-For",
		    "Proxy-Client-IP",
		    "WL-Proxy-Client-IP",
		    "HTTP_X_FORWARDED_FOR",
		    "HTTP_X_FORWARDED",
		    "HTTP_X_CLUSTER_CLIENT_IP",
		    "HTTP_CLIENT_IP",
		    "HTTP_FORWARDED_FOR",
		    "HTTP_FORWARDED",
		    "HTTP_VIA",
		    "REMOTE_ADDR" };
	
	@Autowired
	KorisnikServiceImpl korisnikService;
	
	
	

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse resp = (HttpServletResponse) arg1;

        
        
		resp.setHeader("X-Frame-Options", "SAMEORIGIN");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("X-Content-Type-Options", "nosniff");
		resp.setHeader("X-XSS-Protection", "1; mode=block");
		String reqUrl = req.getRequestURL().toString();
		
		Cookie[] cookies = req.getCookies();
		boolean rutaDozvoljena = false;
		String cookieToken = null;
		String email = null;
		String uloga = null;
		
		if(!reqUrl.startsWith(HOST + "/rest/")){
			//resursi js, css, html...
			arg2.doFilter(arg0, arg1);
			return;
		}
		else if(reqUrl.startsWith(HOST + "/rest/korisnik/logout")){
			arg2.doFilter(arg0, arg1);
			return;
		}
		else{
			for(String ruta:slobodneRute){
        		if(reqUrl.startsWith(HOST + ruta)){
        			arg2.doFilter(arg0, arg1);
        			return;
        		}
        	}
		}

		// ---- pocetak validacije broja poziva u definisanom vremenu
		String ipAddress = getClientIpAddress(req); 
		   
	    if(requestInfoMap.containsKey(ipAddress)){
	    	
	    	long lastAttemptTime = requestInfoMap.get(ipAddress).getLastTime().getTime();
	        Date compareDate = new Date(lastAttemptTime + (LOGGING_BAN_MINUTES * ONE_MINUTE_IN_MILLIS));
	        
	    	if(compareDate.after(new Date())){	    		
	    		requestInfoMap.get(ipAddress).setAttemptNum(requestInfoMap.get(ipAddress).getAttemptNum() + 1);
	    		if(requestInfoMap.get(ipAddress).getAttemptNum() > MAX_REQUESTS){
	    			resp.reset();
	    		    resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		    	}
	    	}
	    	else{
	    		requestInfoMap.get(ipAddress).setLastTime(new Date());
	    		requestInfoMap.get(ipAddress).setAttemptNum(1);
	    	}
	    }
	    else{
		    LoginValidator lv = new LoginValidator(ipAddress, new Date());
		    requestInfoMap.put(ipAddress, lv);
	    }
	    // ---- kraj validacije broja poziva u definisanom vremenu
		
		
        if (cookies != null && !rutaDozvoljena){
          for (Cookie ck : cookies) {
            if (ck.getName().equals("role")) {
            	uloga = ck.getValue();
                if(ck.getValue().equals(ULOGA_GRADJANIN)){
                	for(RequestValidator rv:map.get(ULOGA_GRADJANIN)){
                		if(reqUrl.startsWith(HOST + rv.getName()) && req.getMethod().equals(rv.getMethod())){
                			rutaDozvoljena = true;
                			break;
                		}
                	}
                }
                else if(ck.getValue().equals(ULOGA_ODBORNIK)){
                	for(RequestValidator rv:map.get(ULOGA_GRADJANIN)){
                		if(reqUrl.startsWith(HOST + rv.getName()) && req.getMethod().equals(rv.getMethod())){
                			rutaDozvoljena = true;
                			break;
                		}
                	}
                	if(!rutaDozvoljena){
                		for(RequestValidator rv:map.get(ULOGA_ODBORNIK)){
                    		if(reqUrl.startsWith(HOST + rv.getName()) && req.getMethod().equals(rv.getMethod())){
                    			rutaDozvoljena = true;
                    			break;
                    		}
                    	}
                	}
                }
                else if(ck.getValue().equals(ULOGA_PREDSEDNIK)){
                	rutaDozvoljena = true;
                }
                
                //break; //zbog tokena zakomentarisano
            }
            else if(ck.getName().equals("token")){
            	cookieToken = ck.getValue();
            }
            else if(ck.getName().equals("email")){
            	email = ck.getValue();
            }
          }
        }
        else if(cookies == null){
        	for(String ruta:slobodneRute){
        		if(reqUrl.startsWith(HOST + ruta)){
        			arg2.doFilter(arg0, arg1);
        			return;
        		}
        	}
        }
        
        String token = req.getHeader("Authorization");
        boolean tokenValid = (cookieToken == null && token != null) || (cookieToken != null && token!= null && token.equals(cookieToken)) || (cookieToken != null && token == null);
        
        if(rutaDozvoljena && AuthorizationManager.getInstance().isUserValid(email, token, korisnikService, uloga.equals("gradjanin")) && tokenValid){
        	arg2.doFilter(arg0, arg1);
        }
        else{
        	resp.reset();
		    resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        return;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		slobodneRute = new ArrayList<String>();
		slobodneRute.add("/rest/korisnik/login");
		slobodneRute.add("/rest/korisnik/register");
		slobodneRute.add("/rest/korisnik/numberOfRestAttempts");
		slobodneRute.add("/rest/korisnik/nextLoginDate");
		
		map = new HashMap<String,ArrayList<RequestValidator>>();
		ArrayList<RequestValidator> gradjaninRest = new ArrayList<RequestValidator>();
		ArrayList<RequestValidator> odbornikRest = new ArrayList<RequestValidator>();
		
		gradjaninRest.add(new RequestValidator("/rest/html/amandman"));
		gradjaninRest.add(new RequestValidator("/rest/amandman")); // takodje i /id
		gradjaninRest.add(new RequestValidator("/rest/amandman/propis"));
		gradjaninRest.add(new RequestValidator("/rest/amandman/search", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/amandman/search/meta", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik/register", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik/login", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik/logout", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik/numberOfRestAttempts"));
		gradjaninRest.add(new RequestValidator("/rest/korisnik/nextLoginDate"));
		gradjaninRest.add(new RequestValidator("/rest/html/propis"));
		gradjaninRest.add(new RequestValidator("/rest/propis"));
		gradjaninRest.add(new RequestValidator("/rest/propis/collection"));
		gradjaninRest.add(new RequestValidator("/rest/propis/search", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/propis/search/meta", "POST"));
		gradjaninRest.add(new RequestValidator("/rest/sednica/sednicaZapoceta"));
		
		odbornikRest.add(new RequestValidator("/rest/amandman", "POST"));
		odbornikRest.add(new RequestValidator("/rest/amandman", "DELETE"));
		odbornikRest.add(new RequestValidator("/rest/amandman", "PUT"));
		odbornikRest.add(new RequestValidator("/rest/korisnik", "DELETE"));
		odbornikRest.add(new RequestValidator("/rest/korisnik", "PUT"));
		odbornikRest.add(new RequestValidator("/rest/propis", "POST"));
		odbornikRest.add(new RequestValidator("/rest/propis", "DELETE"));
		odbornikRest.add(new RequestValidator("/rest/propis", "PUT"));
		odbornikRest.add(new RequestValidator("/rest/propis/ponovnoPredlaganje", "PUT"));

		map.put(ULOGA_GRADJANIN, gradjaninRest);
		map.put(ULOGA_ODBORNIK, odbornikRest);
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}
	
	private String getClientIpAddress(HttpServletRequest request) {
	    for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	            return ip;
	        }
	    }
	    return request.getRemoteAddr();
	}

}
