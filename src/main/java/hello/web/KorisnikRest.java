package hello.web;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import hello.dto.LoginPodaci;
import hello.service.KorisnikService;
import hello.utils.AuthorizationManager;
import helpers.LoginValidator;
import helpers.RegexValidator;
import rs.gov.parlament.korisnici.TKorisnik;
import security.passwords.PasswordManager;

@RestController
@RequestMapping(value = "/rest/korisnik")
public class KorisnikRest {
	
	@Autowired
	private KorisnikService korisnikService;
	final static Logger logger = LoggerFactory.getLogger(KorisnikRest.class);
	@Autowired 
	private HttpServletRequest request;
	
	private String passwordRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
	private String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private final long ONE_MINUTE_IN_MILLIS = 60000;
	private final int LOGGING_BAN_MINUTES = 10;
	private final String[] HEADERS_TO_TRY = { 
		    "X-Forwarded-For",
		    "Proxy-Client-IP",
		    "WL-Proxy-Client-IP",
		    "HTTP_X_FORWARDED_FOR",
		    "HTTP_X_FORWARDED",
		    "HTTP_X_CLUSTER_CLIENT_IP",
		    "HTTP_CLIENT_IP",
		    "HTTP_FORWARDED_FOR",
		    "HTTP_FORWARDED",
		    "HTTP_VIA",
		    "REMOTE_ADDR" };
	
	private HashMap<String, LoginValidator> failedLoginAttempts = new HashMap<String,LoginValidator>();
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TKorisnik> getAll() {
		return korisnikService.findAll();
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public Response insert(@RequestBody TKorisnik korisnik) {
		boolean isEmailUnique = true;
		List<TKorisnik> korisnici = getAll();
		
		if(korisnik.getEmail() == null || korisnik.getIme() == null || korisnik.getPassword() == null || korisnik.getPrezime() == null){
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(korisnik.getEmail().length() > 35 || korisnik.getIme().length() > 20 || korisnik.getPrezime().length() > 20 || korisnik.getPassword().length() > 20 || korisnik.getPassword().length() < 8){
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		for(TKorisnik kor:korisnici){
			if(kor.getEmail().equals(korisnik.getEmail())){
				isEmailUnique = false;
				break;
			}
		}
		
		if(isEmailUnique){
			if(!RegexValidator.validateValue(emailRegex, korisnik.getEmail()) || !RegexValidator.validateValue(passwordRegex, korisnik.getPassword())){
				Response.status(Status.BAD_REQUEST).build(); //email or password doesn't match with regex expression
			}
			
		    try {
		    	korisnik.setSalt(PasswordManager.generateSalt());
		    	String token = AuthorizationManager.getInstance().buildToken(korisnik.getEmail(), korisnik.getPassword(), true);
				korisnik.setPassword(PasswordManager.generateHashedPassword(korisnik.getPassword(), PasswordManager.base64Decode(korisnik.getSalt())));
				korisnik.setUloga("gradjanin");
				
				korisnik.setToken(token);
				korisnik.setId(Long.toString(System.currentTimeMillis()));
				
				korisnikService.create(korisnik);
			} catch (IOException e) {
				logger.error(e.getMessage());
				return Response.status(Status.BAD_REQUEST).build();
			}
		}
		else{
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
			
		return Response.status(Status.OK)
				.header("Authorization", korisnik.getToken())
				.header("Role", korisnik.getUloga())
				.build();
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Response login(@RequestBody LoginPodaci p) {
		
		String ipAddress = getClientIpAddress(); 
	   
	    if(failedLoginAttempts.containsKey(ipAddress)){
	    	
	    	long lastAttemptTime = failedLoginAttempts.get(ipAddress).getLastTime().getTime();
	        Date compareDate = new Date(lastAttemptTime + (LOGGING_BAN_MINUTES * ONE_MINUTE_IN_MILLIS));
	        
	    	if(compareDate.before(new Date())){	    		
	    		failedLoginAttempts.get(ipAddress).setAttemptNum(0);
	    	}
	    	else{
	    		if(failedLoginAttempts.get(ipAddress).getAttemptNum() > 4){
		    		return Response.status(Status.FORBIDDEN).build();
		    	}
	    	}
	    }
	    else{
		    LoginValidator lv = new LoginValidator(ipAddress, new Date());
		    failedLoginAttempts.put(ipAddress, lv);
	    }
	    

		TKorisnik temp = korisnikService.findKorisnikByUsername(p.username);
		if(temp != null && PasswordManager.authenticateUser(p.password, temp.getPassword(), temp.getSalt())){
			//ukoliko nadje korisnika u bazi sa tim username-om i passwordom
			//kreira se token i stavlja se u bazu
			//kada se korisnik iloguje token ce se izbrisati u bazi
			String token = AuthorizationManager.getInstance().buildToken(p.username, p.password, temp.getUloga().equals("gradjanin"));
			temp.setToken(token);
			korisnikService.update(temp);
			failedLoginAttempts.get(ipAddress).setAttemptNum(0);
			
			return Response.status(Status.OK)
					.header("Authorization", token)
					.header("Role", temp.getUloga())
					.build();
			
			
		}
		else{
		   failedLoginAttempts.get(ipAddress).setAttemptNum(failedLoginAttempts.get(ipAddress).getAttemptNum() + 1);
		   failedLoginAttempts.get(ipAddress).setLastTime(new Date());
		   
		   return Response.status(Status.BAD_REQUEST).build();
		}
			
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public TKorisnik getOne(@PathVariable("id") String id) {
		return korisnikService.findById(id);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean remove(@PathVariable("id") String id) {
		return korisnikService.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Boolean update(@RequestBody TKorisnik t) {
		return korisnikService.update(t);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public Response logout() {
		String token = request.getHeader("Authorization");
		String username = AuthorizationManager.getInstance().getUsernameFromToken(token);
		TKorisnik temp = korisnikService.findKorisnikByUsername(username);
		if(temp == null)
			return Response.status(Status.UNAUTHORIZED).build();
	
		temp.setToken("");
		korisnikService.update(temp);
		
		return Response.ok().build();
	}
		
	@RequestMapping(value = "/numberOfRestAttempts", method = RequestMethod.GET)
	public int numberOfRestAttempts() {
		String ipAddress = getClientIpAddress(); 
		   
	    if(failedLoginAttempts.containsKey(ipAddress)){
	    	return 5 - failedLoginAttempts.get(ipAddress).getAttemptNum();
	    }
	    
	    return 5;
	}
	
	@RequestMapping(value = "/nextLoginDate", method = RequestMethod.GET)
	public String nextLoginDate() {
		String ipAddress = getClientIpAddress(); 
		String retVal = "";
		
	    if(failedLoginAttempts.containsKey(ipAddress)){
	    	Format formatter = new SimpleDateFormat("HH:mm");
	    	long lastAttemptTime = failedLoginAttempts.get(ipAddress).getLastTime().getTime();
	        Date compareDate = new Date(lastAttemptTime + ((LOGGING_BAN_MINUTES+1) * ONE_MINUTE_IN_MILLIS));// + 1 zbog prikaza vremena(korisniku se ne prikazuju sekunde)
	    	retVal = formatter.format(compareDate);
	    }
		
	    Gson gson = new Gson();
		return gson.toJson(retVal);
	}
	
	private String getClientIpAddress() {
	    for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	            return ip;
	        }
	    }
	    return request.getRemoteAddr();
	}
}
