package hello.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import hello.dto.ClearDto;
import hello.dto.SearchParamDto;
import hello.dto.SearchParamMetaDto;
import hello.dto.SearchResultsDto;
import hello.dto.SearchResultsMetaDto;
import hello.service.AmandmanService;
import hello.service.PropisService;
import helpers.AmandmanDto;
import helpers.ProccessingSearchResults;
import helpers.PropisDto;
import helpers.PropisHelper;
import helpers.RequestHelper;
import helpers.SearchManagerXQuery;
import helpers.XMLPartialUpdateManager;
import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.propisi.TPropis;
import web.html.XSLFOTransformer;
import xml.database.SchemaValidationManager;
import xml.security.EncryptKEK;

@RestController
@RequestMapping(value = "/rest/propis")
public class PropisRest {
	@Autowired
	PropisService propisService;
	
	@Autowired
	AmandmanService amanService;
	

	final static Logger logger = LoggerFactory.getLogger(PropisRest.class);
	
	@Autowired 
	private HttpServletRequest request;
	
	private ArrayList<PropisHelper> usvojeniPropisi = new ArrayList<PropisHelper>();
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TPropis> getAll() {
		return propisService.findAll();
	}
	
	//staviti media type da je xml!!!!!!!
	@RequestMapping(method = RequestMethod.POST)
	public String insert(@RequestBody ClearDto clearObj) {
		String token = request.getHeader("Authorization");
		
		/*String email = CRLHelper.getEmail(request);
		CRLManager crlManager = CRLManager.getInstance();
		boolean isRevoked = crlManager.isRevoked(email);
		if(!isRevoked){
			//return Response.status(Status.FORBIDDEN).build();
		}*/
		
		JAXBContext jaxbContext;
		TPropis noviPropis = null;
		try {
			jaxbContext = JAXBContext.newInstance(TPropis.class);
			//Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Unmarshaller unmarshaller = SchemaValidationManager.getPropisUnmarshaller();
			//System.out.println("instancirn unmarsall: " + unmarshaller);
			StringReader reader = new StringReader(clearObj.getParam());
			//System.out.println("reader: " + reader);
			noviPropis = (TPropis) (TPropis)JAXBIntrospector.getValue(unmarshaller.unmarshal(reader));
		} catch (JAXBException e) {
			logger.error(e.getMessage());
		}
		
		/*if (noviPropis == null) {
			System.out.println("objekat je null");
		} else {
			System.out.println("nziv objekta: " + noviPropis.getNaziv());
		}*/
		
		noviPropis.setId(Long.toString(System.currentTimeMillis()));
		noviPropis.setPotpisnik(token);
		
		//propisService.create(noviPropis);
		propisService.createAuthorizedPropis(noviPropis, RequestHelper.getEmail(request));
		
		return "";
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public TPropis getOne(@PathVariable("id") String id) {
		return propisService.findById(id);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean remove(@PathVariable("id") String id) {
		return propisService.delete(id);
	}
	
	
	
	//staviti media type da je xml!!!!!!!
	@RequestMapping(method = RequestMethod.PUT)
	public Boolean update(@RequestBody TPropis prop) {
		return propisService.update(prop);
	}
	
	@RequestMapping(value = "/collection/{coll}", method = RequestMethod.GET)
	public List<PropisDto> getAllInCollection(@PathVariable("coll") String coll) {
		List<PropisDto> list = SearchManagerXQuery.getInstance().findPropisDtoCollection(coll);
		for(PropisDto propis:list){
			boolean flag = false;
			for(PropisHelper pro:usvojeniPropisi){
				if(propis.getId().equals(pro.getId())){
					flag = true;
					break;
				}
			}
			if(!flag){
				usvojeniPropisi.add(new PropisHelper(propis.getId(), false));
			}
		}
		
		return list;
		
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<SearchResultsDto> getSearch(@RequestBody SearchParamDto param) {
		List<SearchResultsDto> ret = new ArrayList<SearchResultsDto>();
		
		if (param.getElement().equals("")) {
			ret = ProccessingSearchResults.getInstance().search(
					ProccessingSearchResults.COLLECTION_PROPISI,
					param.getValue());
		} else {
			ret = ProccessingSearchResults.getInstance().search(
					ProccessingSearchResults.COLLECTION_PROPISI,
					param.getValue(), param.getElement());
		}
		
		return ret;
	}
	
	@RequestMapping(value = "/search/meta", method = RequestMethod.POST)
	public List<SearchResultsMetaDto> getSearchMeta(@RequestBody SearchParamMetaDto param) {
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		ret = SearchManagerXQuery.getInstance().searchForPropisMethadata(
				param.getDatumPredlaganja(), 
				param.getDatumUsvajanja(), 
				param.getNaslov());
		
		return ret;
	}
	
	@RequestMapping(value = "/ponovnoPredlaganje/{id}", method = RequestMethod.PUT)
	public Boolean reactivatePropis(@PathVariable("id") String id) {
		TPropis propisObj = propisService.findById(id);
		if (propisObj == null) {
			return false;
		}
		propisObj.setUsvojen("predlozen");
		return propisService.update(propisObj);
	}
	
	@RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET)
	public void getPDFFile(
	    @PathVariable("id") String id, 
	    HttpServletResponse response) {
	    try {
	    	
	    	try {
				XSLFOTransformer xslfot = new XSLFOTransformer();
				try {
					xslfot.generatePDFPropis(propisService.createPropisFileWithId(id));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
	    	
		      // get your file as InputStream
		      InputStream is = new FileInputStream(new File("tempFiles/propis"+id+".pdf"));
		      // copy it to response's OutputStream
		      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
		      response.flushBuffer();
	    } catch (IOException ex) {
	    	logger.error(ex.getMessage());
	    }

	}
	
	@RequestMapping(value = "/usvojeniPropisi/{id}", method = RequestMethod.POST)
	public ArrayList<PropisHelper> usvojeniPropisi(@PathVariable("id") String id) {
		for(PropisHelper pro:usvojeniPropisi){
			if(pro.getId().equals(id)){
				pro.setDisabled(true);
				break;
			}
		}
		return usvojeniPropisi;
	}
	
	@RequestMapping(value = "/usvojeniPropisi", method = RequestMethod.GET)
	public ArrayList<PropisHelper> usvojeniPropisi() {
		return usvojeniPropisi;
	}
	
	
	
	@RequestMapping(value = "/sednica", method = RequestMethod.POST)
	public Boolean closeSednica(@RequestBody ArrayList<PropisDto> param) {
		Boolean ret = true;
		for (PropisDto propDto : param) {
			List<AmandmanDto> amandmaniPropisa = SearchManagerXQuery.getInstance().findAmandmaniOdredjenogPropisaDto(propDto.getId());
			for (AmandmanDto amanDto : amandmaniPropisa) {
				if (amanDto.isUsvojen()) {
					TAmandman amanObj = amanService.findById(amanDto.getId());
					try {
						XMLPartialUpdateManager.executeAmandman(amanObj);
					} catch (FileNotFoundException e) {
						logger.error(e.getMessage());
					}
					amanService.delete(amanObj.getId());
				}
				
			}
			TPropis propisFullObj = propisService.findById(propDto.getId());
			propisFullObj.setUsvojen("uCelini");
			propisService.update(propisFullObj);
			

			//TODO: ENKRIPTOVATI PROPIS I POSLATI GA NA ARHIV!!!
			//***********u bazu upisati xml file, a ne handle!!!
			Marshaller mars = SchemaValidationManager.getPropisMarshaller();
			if (mars == null)
				return null;
			File fileTemp = new File("encFile" + (new Date().getTime()));
			try {
				mars.marshal(propisFullObj, fileTemp);
			} catch (JAXBException e1) {
				// TODO Auto-generated catch block
				logger.error(e1.getMessage());
			}
			//****************enkripcija fajla!!! ********
			File encFileTemp = EncryptKEK.getInstance().encryptDocument(fileTemp, "iagns");
			
			String stringParam = "";
			try {
				stringParam = FileUtils.readFileToString(encFileTemp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
			//System.out.println("@@@@@@@@@@@@@@ ispis enkripcije:");
			//System.out.println(stringParam);
			//********************************************
			
			ClearDto clearObj = new ClearDto();
			clearObj.setParam(stringParam);
			
			
			HttpClient client2 = new DefaultHttpClient();
			HttpPost post = new HttpPost("http://localhost:8081//arhiv/propis");
			  //HttpPost post = new HttpPost('https://restUrl');
			  //StringEntity input = new StringEntity('product');
			  //post.setEntity(input);
			StringEntity input;
			try {
				input = new StringEntity(stringParam);
				post.setEntity(input);
				HttpResponse response2 = client2.execute(post);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				logger.error(e1.getMessage());
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
			
		}
		return ret;
	}
	
	
}
