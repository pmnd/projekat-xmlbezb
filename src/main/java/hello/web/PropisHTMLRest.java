package hello.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import hello.service.PropisService;
import web.html.XSLFOTransformer;

@RestController
@RequestMapping(value = "/rest/html/propis")
public class PropisHTMLRest {
	
	@Autowired
	PropisService propisService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getOne(@PathVariable("id") String id) {
		Gson gson = new Gson();
		return gson.toJson(
				XSLFOTransformer.generateHTMLPropis(propisService.getFileById(id))
				);
	}

}
