package hello.service;

import java.util.List;


public interface CrudService<T> {
	
	String create(T t);
	List<T> findAll();
	T findById(String id);
	Boolean update(T t);
	Boolean delete(String id);

}
