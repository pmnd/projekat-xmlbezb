package hello.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.InputStreamHandle;

import rs.gov.parlament.sednice.TListaSednica;
import rs.gov.parlament.sednice.TSednica;
import xml.database.DatabaseManager;
import xml.database.SchemaValidationManager;

@Service
public class SednicaServiceImpl implements SednicaService {
	
	private boolean sednicaZapoceta = false;
	
	@Override
	public String create(TSednica t) {
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		//@@@@@@@@@@@@@@@@ GENERATOR ID-A!!!
		//T,SETID(generisani_id);
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = "/sednice/sednica.xml";
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaSednica listaSednica = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaSednicaUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaSednica = (TListaSednica)SchemaValidationManager.getListaSednicaUnmarshaller().unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaSednica == null) {
			listaSednica = new TListaSednica();
		}
		//System.out.println("nakon obrade listaSednica: " + listaSednica);
		if (!SchemaValidationManager.validateObject(listaSednica))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		listaSednica.getSednica().add(t);
		//System.out.println("dodata sednica '" + t.getOpis() + "' u listu!!!");
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
		//***********u bazu upisati xml file, a ne handle!!!
		Marshaller mars = SchemaValidationManager.getListaSednicaMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("file" + (new Date().getTime()));
		try {
			mars.marshal(listaSednica, fileTemp);
			InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
			docMgr.write(docId, handle);
			Files.deleteIfExists(fileTemp.toPath());
		} catch (Exception e) {
			try {
				Files.deleteIfExists(fileTemp.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
				//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
			}
			return null;
		}
		//**************************************************
		//System.out.println("upisan dokument " + docId + " u bazu!!!");
		
	
		return t.getId();
	}

	@Override
	public List<TSednica> findAll() {
		List<TSednica> ret = new ArrayList<TSednica>();
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = "/sednice/sednica.xml";
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaSednica listaSednica = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaSednicaUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaSednica = (TListaSednica)SchemaValidationManager.getListaSednicaUnmarshaller().unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (listaSednica == null) {
			listaSednica = new TListaSednica();
		}
		//System.out.println("nakon obrade listaSednica: " + listaSednica);
		if (!SchemaValidationManager.validateObject(listaSednica))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		ret = listaSednica.getSednica();
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		return ret;
	}

	@Override
	public TSednica findById(String id) {
		TSednica ret = null;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = "/sednice/sednica.xml";
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaSednica listaSednica = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaSednicaUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaSednica = (TListaSednica)SchemaValidationManager.getListaSednicaUnmarshaller().unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaSednica == null) {
			listaSednica = new TListaSednica();
		}
		//System.out.println("nakon obrade listaSednica: " + listaSednica);
		if (!SchemaValidationManager.validateObject(listaSednica))
			return null;

		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		for (TSednica x : listaSednica.getSednica()) {
			if (x.getId().equals(id)) {
				ret = x;
				break;
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
		return ret;
	}

	@Override
	public Boolean update(TSednica t) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = "/sednice/sednica.xml";
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaSednica listaSednica = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaSednicaUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaSednica = (TListaSednica)SchemaValidationManager.getListaSednicaUnmarshaller().unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaSednica == null) {
			listaSednica = new TListaSednica();
		}
		//System.out.println("nakon obrade listaSednica: " + listaSednica);
		if (!SchemaValidationManager.validateObject(listaSednica))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		TListaSednica izmenjenaLista = new TListaSednica();
		
		for (TSednica x : listaSednica.getSednica()) {
			if (x.getId().equals(t.getId())) {
				izmenjenaLista.getSednica().add(t);
				ret = true;
			} else {
				izmenjenaLista.getSednica().add(x);
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (ret) {
			//***********u bazu upisati xml file, a ne handle!!!
			Marshaller mars = SchemaValidationManager.getListaSednicaMarshaller();
			if (mars == null)
				return null;
			File fileTemp = new File("file" + (new Date().getTime()));
			try {
				mars.marshal(izmenjenaLista, fileTemp);
				InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
				docMgr.write(docId, handle);
				Files.deleteIfExists(fileTemp.toPath());
			} catch (Exception e) {
				try {
					Files.deleteIfExists(fileTemp.toPath());
				} catch (IOException e1) {
					e1.printStackTrace();
					//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
				}
				return null;
			}
			//**************************************************
			//System.out.println("upisan dokument " + docId + " u bazu!!!");
		}
		
		return ret;
	}

	@Override
	public Boolean delete(String id) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = "/sednice/sednica.xml";
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaSednica listaSednica = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaSednicaUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaSednica = (TListaSednica)SchemaValidationManager.getListaSednicaUnmarshaller().unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaSednica == null) {
			listaSednica = new TListaSednica();
		}
		//System.out.println("nakon obrade listaSednica: " + listaSednica);
		if (!SchemaValidationManager.validateObject(listaSednica))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		TListaSednica izmenjenaLista = new TListaSednica();
		
		for (TSednica x : listaSednica.getSednica()) {
			if (x.getId().equals(id)) {
				ret = true;
			} else {
				izmenjenaLista.getSednica().add(x);
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (ret) {
			//***********u bazu upisati xml file, a ne handle!!!
			Marshaller mars = SchemaValidationManager.getListaSednicaMarshaller();
			if (mars == null)
				return null;
			File fileTemp = new File("file" + (new Date().getTime()));
			try {
				mars.marshal(izmenjenaLista, fileTemp);
				InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
				docMgr.write(docId, handle);
				Files.deleteIfExists(fileTemp.toPath());
			} catch (Exception e) {
				try {
					Files.deleteIfExists(fileTemp.toPath());
				} catch (IOException e1) {
					e1.printStackTrace();
					//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
				}
				return null;
			}
			//**************************************************
			//System.out.println("upisan dokument " + docId + " u bazu!!!");
		}
		
		return ret;
	}

	@Override
	public boolean isSednicaZapoceta() {
		return sednicaZapoceta;
	}

	@Override
	public void setSednicaZapoceta(boolean isSednicaZapoceta) {
		sednicaZapoceta = isSednicaZapoceta;
	}
}
