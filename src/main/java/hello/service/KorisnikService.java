package hello.service;

import rs.gov.parlament.korisnici.TKorisnik;

public interface KorisnikService extends CrudService<TKorisnik>{

	
	TKorisnik findKorisnikByUsername(String username);
}
