package hello.service;

import java.io.File;

import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.propisi.TPropis;

public interface AmandmanService extends CrudService<TAmandman> {
	File getFileById(String id);
	File createAmanFileWithId(String id);
	String createAuthorizedAmandman(TAmandman t, String username);
}
