package hello.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.InputStreamHandle;

import rs.gov.parlament.korisnici.TKorisnik;
import rs.gov.parlament.korisnici.TListaKorisnika;
import xml.database.DatabaseManager;
import xml.database.SchemaValidationManager;

@Service
public class KorisnikServiceImpl implements KorisnikService {


	private static final String DOC_ID = "/korisnici/korisnik.xml";

	@Override
	public String create(TKorisnik t) {
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		//@@@@@@@@@@@@@@@@ GENERATOR ID-A!!!
		//T,SETID(generisani_id);
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu korisnika u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		listaKorisnika.getKorisnik().add(t);
		//System.out.println("dodat korisnik '" + t.getEmail() + "' u listu!!!");
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
		//***********u bazu upisati xml file, a ne handle!!!
		Marshaller mars = SchemaValidationManager.getListaKorisnikMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("file" + (new Date().getTime()));
		try {
			mars.marshal(listaKorisnika, fileTemp);
			InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
			docMgr.write(docId, handle);
			Files.deleteIfExists(fileTemp.toPath());
		} catch (Exception e) {
			try {
				Files.deleteIfExists(fileTemp.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
				//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
			}
			return null;
		}
		//**************************************************
		//System.out.println("upisan dokument " + docId + " u bazu!!!");
		
	
		return t.getId();
	}

	@Override
	public List<TKorisnik> findAll() {
		List<TKorisnik> ret = new ArrayList<TKorisnik>();
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		ret = listaKorisnika.getKorisnik();
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		return ret;
	}

	@Override
	public TKorisnik findById(String id) {
		TKorisnik ret = null;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;

		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		for (TKorisnik x : listaKorisnika.getKorisnik()) {
			if (x.getId().equals(id)) {
				ret = x;
				break;
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
		return ret;
	}

	@Override
	public Boolean update(TKorisnik t) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		TListaKorisnika izmenjenaLista = new TListaKorisnika();
		
		for (TKorisnik x : listaKorisnika.getKorisnik()) {
			if (x.getId().equals(t.getId())) {
				izmenjenaLista.getKorisnik().add(t);
				ret = true;
			} else {
				izmenjenaLista.getKorisnik().add(x);
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (ret) {
			//***********u bazu upisati xml file, a ne handle!!!
			Marshaller mars = SchemaValidationManager.getListaKorisnikMarshaller();
			if (mars == null)
				return null;
			File fileTemp = new File("file" + (new Date().getTime()));
			try {
				mars.marshal(izmenjenaLista, fileTemp);
				InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
				docMgr.write(docId, handle);
				Files.deleteIfExists(fileTemp.toPath());
			} catch (Exception e) {
				try {
					Files.deleteIfExists(fileTemp.toPath());
				} catch (IOException e1) {
					e1.printStackTrace();
					//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
				}
				return null;
			}
			//**************************************************
			//System.out.println("upisan dokument " + docId + " u bazu!!!");
		}
		
		return ret;
	}

	@Override
	public Boolean delete(String id) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();
		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu korisnika u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;
		
		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		TListaKorisnika izmenjenaLista = new TListaKorisnika();
		
		for (TKorisnik x : listaKorisnika.getKorisnik()) {
			if (x.getId().equals(id)) {
				ret = true;
			} else {
				izmenjenaLista.getKorisnik().add(x);
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		if (ret) {
			//***********u bazu upisati xml file, a ne handle!!!
			Marshaller mars = SchemaValidationManager.getListaKorisnikMarshaller();
			if (mars == null)
				return null;
			File fileTemp = new File("file" + (new Date().getTime()));
			try {
				mars.marshal(izmenjenaLista, fileTemp);
				InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
				docMgr.write(docId, handle);
				Files.deleteIfExists(fileTemp.toPath());
			} catch (Exception e) {
				try {
					Files.deleteIfExists(fileTemp.toPath());
				} catch (IOException e1) {
					e1.printStackTrace();
					//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
				}
				return null;
			}
			//**************************************************
			//System.out.println("upisan dokument " + docId + " u bazu!!!");
		}
		
		return ret;
	}

	@Override
	public TKorisnik findKorisnikByUsername(String username) {
		TKorisnik ret = null;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		
		String docId = DOC_ID;
		try {
			//System.out.println("usao u try za read!!!");
			docMgr.read(docId, content);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("onemoguceno citanje " + docId + " iz baze!!!");
		}
		Document doc = content.get();
		TListaKorisnika listaKorisnika = null;
		try {
			Unmarshaller unmars = SchemaValidationManager.getListaKorisnikUnmarshaller();
			if (unmars == null) {
				//System.out.println("unmars je null!!!");
				return null;
			}
			if (doc != null)
				listaKorisnika = (TListaKorisnika)unmars.unmarshal(doc);
			//else
				//System.out.println("doc je null, nije pronasao listu sednica u bazu!");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (listaKorisnika == null) {
			listaKorisnika = new TListaKorisnika();
		}
		//System.out.println("nakon obrade listaKorisnika: " + listaKorisnika);
		if (!SchemaValidationManager.validateObject(listaKorisnika))
			return null;

		//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@
		for (TKorisnik x : listaKorisnika.getKorisnik()) {
			if (x.getEmail().equals(username)) {
				ret = x;
				break;
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
		
		return ret;
	}
}
