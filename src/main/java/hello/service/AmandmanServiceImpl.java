package hello.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.InputStreamHandle;
import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.MatchDocumentSummary;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StringQueryDefinition;

import rs.gov.parlament.amandmani.TAmandman;
import xml.database.DatabaseManager;
import xml.database.SchemaValidationManager;
import xml.security.SignEnveloped;

@Service
public class AmandmanServiceImpl implements AmandmanService {
	
	@Autowired
	SednicaService sednicaService;
	
	private static final String COLLECTION = "/amandmani";
	
	@Override
	public String create(TAmandman t) {
		
		if(sednicaService.isSednicaZapoceta()){
			return null;
		}
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		
		String newFileUri = "amandman" + t.getId() + ".xml";
		
		//******************* citanje svih fajlova iz kolekcije amandmana ***
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		/*String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		queryDefinition.setCriteria(criteria);*/
		
		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			//System.out.println("uri result fajla: " + result.getUri());
			if (result.getUri().equals(newFileUri)) {
				//System.out.println("########Vec postoji fajl sa nazivom " + newFileUri);
				return null;
			}
		}
		
		//System.out.println("zavrsio pretragu, nema tog fajla u bazi");
		
		//***********u bazu upisati xml file, a ne handle!!!
		Marshaller mars = SchemaValidationManager.getAmandmanMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("file" + (new Date().getTime()));
		try {
			mars.marshal(t, fileTemp);
			//****************POTPISIVANJE DOKUMENTA***********
			//staviti na mesto certificate alias-a username korisnika
			fileTemp = SignEnveloped.getInstance().transformToSignedDocument(fileTemp, "primer");
			//*************************************************
			InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.write(newFileUri, metadata, handle);
			Files.deleteIfExists(fileTemp.toPath());
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				Files.deleteIfExists(fileTemp.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
				//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
			}
			return null;
		}
		//System.out.println("upisan dokument " + newFileUri + " u bazu!!!");		
		return t.getId();
	}

	@Override
	public List<TAmandman> findAll() {
		List<TAmandman> ret = new ArrayList<TAmandman>();
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		//******************* citanje svih fajlova iz kolekcije amandmana ***
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		/*String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		queryDefinition.setCriteria(criteria);*/
		
		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			//System.out.println("uri result fajla: " + result.getUri());
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.read(result.getUri(), metadata, content);
			
			try {
				Unmarshaller unmars = SchemaValidationManager.getAmandmanUnmarshaller();
				if (unmars == null) {
					//System.out.println("unmars je null!!!");
					return ret;
				}
				TAmandman elemAman = (TAmandman)unmars.unmarshal(content.get());
				ret.add(elemAman);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return ret;
	}

	@Override
	public TAmandman findById(String id) {
		TAmandman ret = null;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();
		
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		/*String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		queryDefinition.setCriteria(criteria);*/
		
		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			if (!result.getUri().contains(id)) {
				continue;
			}
			//System.out.println("uri result fajla: " + result.getUri());
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.read(result.getUri(), metadata, content);
			
			try {
				Unmarshaller unmars = SchemaValidationManager.getAmandmanUnmarshaller();
				if (unmars == null) {
					//System.out.println("unmars je null!!!");
					return ret;
				}
				TAmandman elemAman = (TAmandman)unmars.unmarshal(content.get());
				if (elemAman.getId().equals(id)) {
					ret = elemAman;
					break;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		return ret;
	}

	@Override
	public Boolean update(TAmandman t) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();
		
		t.setSignature(null);
		
		if (!SchemaValidationManager.validateObject(t))
			return ret;
		
		//t.setSignature(tempSign);
		docMgr = client.newXMLDocumentManager();
		
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		/*String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		queryDefinition.setCriteria(criteria);*/
		
		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			if (!result.getUri().contains(t.getId())) {
				continue;
			}
			//System.out.println("uri result fajla: " + result.getUri());
			//@@@@@@@@@@@@@@@@@ OVDE JE GLAVNA LOGIKA METODE @@@@@@@@@@@@@@@@@@@@@
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.read(result.getUri(), metadata, content);
			
			try {
				Unmarshaller unmars = SchemaValidationManager.getAmandmanUnmarshaller();
				if (unmars == null) {
					//System.out.println("unmars je null!!!");
					return ret;
				}
				TAmandman elemAman = (TAmandman)unmars.unmarshal(content.get());
				if (elemAman.getId().equals(t.getId())) {
					
					//***********u bazu upisati xml file, a ne handle!!!
					Marshaller mars = SchemaValidationManager.getAmandmanMarshaller();
					if (mars == null)
						return ret;
					File fileTemp = new File("file" + (new Date().getTime()));
					try {
						mars.marshal(t, fileTemp);
						InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
						DocumentMetadataHandle metadataUpis = new DocumentMetadataHandle();
						metadataUpis.getCollections().add(COLLECTION);
						docMgr.write(result.getUri(), metadata, handle);
						Files.deleteIfExists(fileTemp.toPath());
					} catch (Exception e) {
						e.printStackTrace();
						try {
							Files.deleteIfExists(fileTemp.toPath());
						} catch (IOException e1) {
							e1.printStackTrace();
							//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
						}
						return null;
					}
					
					//System.out.println("upisan dokument " + result.getUri() + " u bazu!!!");
					ret = true;
					break;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		return ret;
	}

	@Override
	public Boolean delete(String id) {
		Boolean ret = false;
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		
		docMgr = client.newXMLDocumentManager();

		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();

		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		//citanje xml-a iz baze, bez yaxb
		DOMHandle content = new DOMHandle();
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			if (!result.getUri().contains(id)) {
				continue;
			}
			//System.out.println("uri result fajla: " + result.getUri());
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.read(result.getUri(), metadata, content);
			
			try {
				Unmarshaller unmars = SchemaValidationManager.getAmandmanUnmarshaller();
				if (unmars == null) {
					//System.out.println("unmars je null!!!");
					return ret;
				}
				TAmandman elemAman = (TAmandman)unmars.unmarshal(content.get());
				if (elemAman.getId().equals(id)) {
					DocumentMetadataHandle metadataDelete = new DocumentMetadataHandle();
					metadataDelete.getCollections().add(COLLECTION);
					docMgr.delete(result.getUri());
					ret = true;
					break;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return ret;
	}
	
	@Override
	public File getFileById(String id) {
		File ret = null;
		
		Marshaller mars = SchemaValidationManager.getAmandmanMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("file" + (new Date().getTime()));
		
		TAmandman amandmanObj = findById(id);
		
		try {
			mars.marshal(amandmanObj, fileTemp);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ret = fileTemp;
		
		return ret;
	}

	@Override
	public File createAmanFileWithId(String id) {
		File ret = null;
		
		Marshaller mars = SchemaValidationManager.getAmandmanMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("tempFiles/amandman" + id);
		
		TAmandman amandmanObj = findById(id);
		
		try {
			mars.marshal(amandmanObj, fileTemp);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ret = fileTemp;
		
		return ret;
	}

	@Override
	public String createAuthorizedAmandman(TAmandman t, String username) {

		if(sednicaService.isSednicaZapoceta()){
			return null;
		}
		
		DatabaseClient client;
		XMLDocumentManager docMgr;
		client = DatabaseManager.getInstance().getClient();

		if (!SchemaValidationManager.validateObject(t))
			return null;
		
		
		docMgr = client.newXMLDocumentManager();
		
		
		String newFileUri = "amandman" + t.getId() + ".xml";
		
		//******************* citanje svih fajlova iz kolekcije amandmana ***
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		/*String criteria = "angularjs OR test AND \"Kurt Cagle\"";
		queryDefinition.setCriteria(criteria);*/
		
		// Search within a specific collection
		queryDefinition.setCollections(COLLECTION);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		//System.out.println("results: " + results);
		
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		//System.out.println("matches: " + matches);
		MatchDocumentSummary result;
		
		for (int i = 0; i < matches.length; i++) {
			result = matches[i];
			//System.out.println("uri result fajla: " + result.getUri());
			if (result.getUri().equals(newFileUri)) {
				//System.out.println("########Vec postoji fajl sa nazivom " + newFileUri);
				return null;
			}
		}
		
		//System.out.println("zavrsio pretragu, nema tog fajla u bazi");
		
		//***********u bazu upisati xml file, a ne handle!!!
		Marshaller mars = SchemaValidationManager.getAmandmanMarshaller();
		if (mars == null)
			return null;
		File fileTemp = new File("file" + (new Date().getTime()));
		try {
			mars.marshal(t, fileTemp);
			//****************POTPISIVANJE DOKUMENTA***********
			//staviti na mesto certificate alias-a username korisnika
			fileTemp = SignEnveloped.getInstance().transformToSignedDocument(fileTemp, username);
			//*************************************************
			InputStreamHandle handle = new InputStreamHandle(new FileInputStream(fileTemp));
			DocumentMetadataHandle metadata = new DocumentMetadataHandle();
			metadata.getCollections().add(COLLECTION);
			docMgr.write(newFileUri, metadata, handle);
			Files.deleteIfExists(fileTemp.toPath());
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				Files.deleteIfExists(fileTemp.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
				//System.out.println("neuspelo brisanje " + fileTemp.getName() + " iz servisa!");
			}
			return null;
		}
		//System.out.println("upisan dokument " + newFileUri + " u bazu!!!");		
		return t.getId();
	}
	
}
