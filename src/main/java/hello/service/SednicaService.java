package hello.service;

import rs.gov.parlament.sednice.TSednica;

public interface SednicaService extends CrudService<TSednica> {
	boolean isSednicaZapoceta();
	void setSednicaZapoceta(boolean isSednicaZapoceta);
}
