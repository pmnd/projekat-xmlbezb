package hello.service;

import java.io.File;
import java.util.List;

import helpers.PropisDto;
import rs.gov.parlament.propisi.TPropis;

public interface PropisService extends CrudService<TPropis> {
	File getFileById(String id);
	List<PropisDto> findAllDto();
	File createPropisFileWithId(String id);
	String createAuthorizedPropis(TPropis t, String username);
}
