package hello.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="data")
public class ClearDto {
	String param;

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}
}
