package hello.dto;

import java.util.List;

public class SearchResultsDto {
	private String id;
	private List<String> matchResults;
	
	public SearchResultsDto() {
		
	}
	
	public SearchResultsDto(String id, List<String> matchResults) {
		this.id = id;
		this.matchResults = matchResults;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getMatchResults() {
		return matchResults;
	}

	public void setMatchResults(List<String> matchResult) {
		this.matchResults = matchResult;
	}

}
