package hello.dto;

public class SearchResultsMetaDto {
	private String id;
	private String naziv;
	
	public SearchResultsMetaDto() {
		
	}
	
	public SearchResultsMetaDto(String id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
}
