package hello.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="loginCredentials")
public class LoginPodaci {
	public String username;
	public String password;
	public LoginPodaci() {
		super();
	}
	
	public LoginPodaci(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	
}
