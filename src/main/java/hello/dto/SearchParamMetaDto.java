package hello.dto;

public class SearchParamMetaDto {
	private String naslov;
	private String datumPredlaganja;
	private String datumUsvajanja;
	
	public SearchParamMetaDto() {
		
	}
	
	public SearchParamMetaDto(String naslov, String datumPredlaganja, String datumUsvajanja) {
		super();
		this.naslov = naslov;
		this.datumPredlaganja = datumPredlaganja;
		this.datumUsvajanja = datumUsvajanja;
	}

	public String getNaslov() {
		return naslov;
	}
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}
	public String getDatumPredlaganja() {
		return datumPredlaganja;
	}
	public void setDatumPredlaganja(String datumPredlaganja) {
		this.datumPredlaganja = datumPredlaganja;
	}
	public String getDatumUsvajanja() {
		return datumUsvajanja;
	}
	public void setDatumUsvajanja(String datumUsvajanja) {
		this.datumUsvajanja = datumUsvajanja;
	}
}
