package hello.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.Key;

import hello.service.KorisnikServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import rs.gov.parlament.korisnici.TKorisnik;
import xml.security.SignEnveloped;


public class AuthorizationManager {
	private static AuthorizationManager instance;
	private static Key key;
	private final String gradjaninAlias = "gradjanin@gmail.com";
	
	private AuthorizationManager(){
		key = SignEnveloped.getInstance().readPrivateKey();
	}
	
	
	public static AuthorizationManager getInstance(){
		if(instance == null){
			instance = new AuthorizationManager();
		}
		
		return instance;
	}


	public Key getKey() {
		return key;
	}
	
	public String buildToken(String username, String password, boolean isGradjanin){
		Key tempKey = null;
		if(isGradjanin){
			tempKey = SignEnveloped.getInstance().readPrivateKey(gradjaninAlias);
		}
		else{
			tempKey = SignEnveloped.getInstance().readPrivateKey(username);
		}
 
		String retVal = Jwts
				.builder()
				.setSubject("Skupstina")
				.setHeaderParam("username", username)
				.setHeaderParam("password", password)
				.signWith(SignatureAlgorithm.RS512, tempKey)
				.compact();
		
		return retVal;
	}
	
	
	public boolean isUserValid(String email, String token, KorisnikServiceImpl korisnikService, boolean isGradjanin){
		Key tempKey = null;
		if(isGradjanin){
			tempKey = SignEnveloped.getInstance().readPrivateKey(gradjaninAlias);
		}
		else{
			tempKey = SignEnveloped.getInstance().readPrivateKey(email);
		}

		try{
			Jwts.parser().setSigningKey(tempKey).parse(token);
		}catch(Exception e){
			return false;
		}
		String username = (String) Jwts.parser().setSigningKey(tempKey).parseClaimsJws(token).getHeader().get("username");
		//String password = (String) Jwts.parser().setSigningKey(key).parseClaimsJws(token).getHeader().get("password");
		
		TKorisnik korisnik = korisnikService.findKorisnikByUsername(username);
		if(korisnik == null)
			return false;
		if(korisnik.getToken() == null || korisnik.getToken() == "")
			return false;
		if(!korisnik.getToken().equals(token))
			return false;
		
		return true;
	}
	
	public String getUsernameFromToken(String token){
		try{
			Jwts.parser().setSigningKey(key).parse(token);
		}catch(Exception e){
			return "";
		}
		String username = "";
		username = (String) Jwts.parser().setSigningKey(key).parseClaimsJws(token).getHeader().get("username");
		return username;
	}
}
