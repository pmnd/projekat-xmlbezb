package xml.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;

public class CRLManager {
	private X509CRL crl = null;
	private static CRLManager instance;
    
	public static CRLManager getInstance() {
		if (instance == null) {
			instance = new CRLManager();
		}
		return instance;
	}
	
    private CRLManager(){
        Security.addProvider(new BouncyCastleProvider());
        crl = initializeCRL();
    }

    @SuppressWarnings("deprecation")
	public  X509CRL initializeCRL(){
        X509CRL crl = null;
        X509Certificate cert = null;
        try{
            KeyStore store = loadKeyStore("certificates/ucesnici.jks", "1P@ssword".toCharArray());
            PrivateKey pk = (PrivateKey)store.getKey("odbornik3@gmail.com", "1P@ssword".toCharArray()); 
    		cert = (X509Certificate) store.getCertificate("odbornik3@gmail.com");
    		X500Name issuer = new X500Name(PrincipalUtil.getIssuerX509Principal(cert).getName());
    		//X500Name issuer = X500Name.getInstance(PrincipalUtil.getIssuerX509Principal(cert).getEncoded());
    		
            X509v2CRLBuilder crlBuilder = new X509v2CRLBuilder(issuer, new Date());
            
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(2016, 1, 16, 8, 8, 0);
            Date revocationDate = cal.getTime();
            
            
            crlBuilder.addCRLEntry(BigInteger.TEN, revocationDate, CRLReason.keyCompromise);
            crlBuilder.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(cert));
            crlBuilder.addExtension(X509Extensions.CRLNumber, false, new CRLNumber(BigInteger.ONE));
            JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
            builder = builder.setProvider("BC");
            X509CRLHolder crlHolder = crlBuilder.build(builder.build(pk));
            JcaX509CRLConverter converter = new JcaX509CRLConverter();
            converter.setProvider("BC");

            crl = converter.getCRL(crlHolder);

        } catch (Exception ex){
        	ex.printStackTrace();
        }
        return crl;
    }

   
    public boolean isRevoked(String email){
    	KeyStore store = null;
    	Certificate certificate = null;
    	boolean ret = false;
    	
    	/*Set s = crl.getRevokedCertificates();
        if (s != null && s.isEmpty() == false) {
          Iterator t = s.iterator();
          while (t.hasNext()) {
            X509CRLEntry entry = (X509CRLEntry) t.next();
            System.out.println("serial number = " + entry.getSerialNumber().toString(16));
            System.out.println("revocation date = " + entry.getRevocationDate());
            System.out.println("extensions = " + entry.hasExtensions());
          }
        }*/
    	
		try {
			store = loadKeyStore("certificates/ucesnici.jks", "1P@ssword".toCharArray());
			certificate = store.getCertificate(email);
		} catch (KeyStoreException | IOException e) {
			e.printStackTrace();
		}

        if(crl == null)
            return false;
       
        ret = crl.isRevoked(certificate);
        return ret;
    }
    
    private KeyStore loadKeyStore(String fileName, char[] password) throws IOException {
    	KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance("JKS", "SUN");
			if(fileName != null) 
				keyStore.load(new FileInputStream(fileName), password);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			throw e;
		}catch(Exception e){
			e.printStackTrace();
		}
		return keyStore;
	}
}
