package xml.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import rs.gov.parlament.amandmani.TAmandman;
import rs.gov.parlament.korisnici.TKorisnik;
import rs.gov.parlament.korisnici.TListaKorisnika;
import rs.gov.parlament.propisi.TPropis;
import rs.gov.parlament.sednice.TListaSednica;
import rs.gov.parlament.sednice.TSednica;

public abstract class SchemaValidationManager {
	
	public static Marshaller getSednicaMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TSednica.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
					
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getSednicaUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TSednica.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaSednice.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TSednica obj) {
		boolean ret = true;
		File fileTemp = null;
		FileInputStream fis = null;
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TSednica.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
				
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			 
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaSednice.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			obj = (TSednica) unmarshaller.unmarshal(fileTemp);
			fis = new FileInputStream(fileTemp);
			InputStream is = fis;
			//fis.close();
			
			unmarshaller.unmarshal(is);
			
			
			fis.close();
			Files.deleteIfExists(fileTemp.toPath());
			
			
		}
		catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("#########aaaaa NEUSPELA VALIDACIJA SEDNICE!");
			ret = false;
			try {
				fis.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		if (!ret) {
			try {
				fis.close();
				//System.out.println("zatvoren stream od fajla: " + fileTemp.getName());
				Files.deleteIfExists(fileTemp.toPath());
				//System.out.println("obrisan fajl: " + fileTemp.getName());
				
			} catch (IOException e1) {
				e1.printStackTrace();
				//System.out.println("@@@@@@@ NEUSPELO ZATVARANJE FAJLA " + fileTemp.getName() + "!");
			}
		}
		
		return ret;
	}
	
	//*********************************************
	public static Marshaller getListaSednicaMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TListaSednica.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
					
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getListaSednicaUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TListaSednica.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaSednice.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TListaSednica obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TListaSednica.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaSednice.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			obj = (TListaSednica) unmarshaller.unmarshal(fileTemp);
	        Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println("######### NEUSPELA VALIDACIJA SEDNICE!");
			return false;
		}
		
		return ret;
	}
	//*********************************************
	
	public static Marshaller getKorisnikMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TKorisnik.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getKorisnikUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TKorisnik.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaKorisnici.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TKorisnik obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TKorisnik.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaKorisnici.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			obj = (TKorisnik) unmarshaller.unmarshal(fileTemp);
	        Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println("######### NEUSPELA VALIDACIJA KORISNIKA!");
			return false;
		}
		
		return ret;
	}
	
	//***************************************************
	public static Marshaller getListaKorisnikMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TListaKorisnika.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getListaKorisnikUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TListaKorisnika.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaKorisnici.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TListaKorisnika obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TListaKorisnika.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaKorisnici.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			obj = (TListaKorisnika) unmarshaller.unmarshal(fileTemp);
	        Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println("######### NEUSPELA VALIDACIJA KORISNIKA!");
			return false;
		}
		
		return ret;
	}
	//***************************************************
	
	public static Marshaller getAmandmanMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TAmandman.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Marshaller getAmandmanFreeMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TAmandman.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getAmandmanUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TAmandman.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaAmandmani.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TAmandman obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TAmandman.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaAmandmani.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			obj = (TAmandman) unmarshaller.unmarshal(fileTemp);
	        Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println("######### NEUSPELA VALIDACIJA AMANDMANA!");
			return false;
		}
		
		return ret;
	}

	public static Marshaller getPropisMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getPropisUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaPropisi.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TPropis obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaPropisi.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			Object o = unmarshaller.unmarshal(fileTemp);
			Object o2 = JAXBIntrospector.getValue(unmarshaller.unmarshal(fileTemp));
			/*System.out.println("unmarsalovan objekat:");
			System.out.println(o);
			System.out.println(o.getClass());
			System.out.println(o2);
			System.out.println(o2.getClass());*/
			//obj = (TPropis) unmarshaller.unmarshal(fileTemp);
			obj = (TPropis) JAXBIntrospector.getValue(unmarshaller.unmarshal(fileTemp));
			Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println("######### NEUSPELA VALIDACIJA PROPISA!");
			return false;
		}
		
		return ret;
	}
	
}
