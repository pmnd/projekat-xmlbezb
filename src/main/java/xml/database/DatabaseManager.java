package xml.database;

import java.io.IOException;

import javax.xml.transform.TransformerFactory;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.util.EditableNamespaceContext;

import xml.database.Util.ConnectionProperties;

public class DatabaseManager {
	
	private static DatabaseClient client;
	private static TransformerFactory transformerFactory;
	private static DatabaseManager instance;
	
	private static EditableNamespaceContext namespaces;
	
	static {
		namespaces = new EditableNamespaceContext();
		//namespaces.put("prop", "http://www.parlament.gov.rs/propisi");
		namespaces.put("aman", "http://www.parlament.gov.rs/amandmani");
		namespaces.put("sed", "http://www.parlament.gov.rs/sednice");
		namespaces.put("kor", "http://www.parlament.gov.rs/korisnici");
	}
	
	private DatabaseManager(){
		ConnectionProperties props = null;
		
		try {
			props = Util.loadProperties();
		} catch (IOException e) {
			e.printStackTrace();
		}
		client = DatabaseClientFactory.newClient(props.host, props.port, props.database , props.user, props.password, Authentication.DIGEST);
		transformerFactory = TransformerFactory.newInstance();
	}
	
	
	public static DatabaseManager getInstance(){
		if(instance == null){
			instance = new DatabaseManager();
			return instance;
		}else
			return instance;
	}
	
	

	public  DatabaseClient getClient() {
		
		return client;
	}

	public static EditableNamespaceContext getNamespaces() {
		return namespaces;
	}


	public  TransformerFactory getTransformerFactory() {
		return transformerFactory;
	}
	
	
	public static void main(String [] args){
			DatabaseClient c = DatabaseManager.getInstance().getClient();
	}
}
