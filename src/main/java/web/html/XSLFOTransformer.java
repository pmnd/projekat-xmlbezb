package web.html;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

import net.sf.saxon.TransformerFactoryImpl;

/**
 * 
 * Primer demonstrira koriscenje programskog API-a za 
 * renderovanje PDF-a na osnovu XSL-FO transformacije.
 *
 */
public class XSLFOTransformer {
	
	private FopFactory fopFactory;
	
	private TransformerFactory transformerFactory;
	
	private static final String FOP_FACTORY_FILE_PATH = "./src/main/java/data/fop.xconf";
	private static final String PROPIS_XSL_FILE_PATH = "./src/main/java/data/propis.xsl";
	private static final String AMANDMAN_XSL_FILE_PATH = "./src/main/java/data/amandman.xsl";
	private static final String AMANDMAN_FO_XSL_FILE_PATH = "./src/main/java/data/amandmani_fo.xsl";
	private static final String PROPIS_FO_XSL_FILE_PATH = "./src/main/java/data/propis_fo.xsl";
	
	private XSLFOTransformer instance;
	
	
	public XSLFOTransformer() throws SAXException, IOException {
		
		// Initialize FOP factory object
		fopFactory = FopFactory.newInstance(new File(FOP_FACTORY_FILE_PATH));
		
		// Setup the XSLT transformer factory
		transformerFactory = new TransformerFactoryImpl();
	}

	private void test() throws Exception {
		
		//System.out.println("[INFO] " + XSLFOTransformer.class.getSimpleName());
		
		// Point to the XSL-FO file
		File xsltFile = new File("data/xsl-fo/bookstore_fo.xsl");

		// Create transformation source
		StreamSource transformSource = new StreamSource(xsltFile);
		
		// Initialize the transformation subject
		StreamSource source = new StreamSource(new File("data/xsl-fo/bookstore.xml"));

		// Initialize user agent needed for the transformation
		FOUserAgent userAgent = fopFactory.newFOUserAgent();
		
		// Create the output stream to store the results
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		// Initialize the XSL-FO transformer object
		Transformer xslFoTransformer = transformerFactory.newTransformer(transformSource);
		
		// Construct FOP instance with desired output format
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream);

		// Resulting SAX events 
		Result res = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		xslFoTransformer.transform(source, res);

		// Generate PDF file
		File pdfFile = new File("tempFiles/.pdf");
		OutputStream out = new BufferedOutputStream(new FileOutputStream(pdfFile));
		out.write(outStream.toByteArray());

		//System.out.println("[INFO] File \"" + pdfFile.getCanonicalPath() + "\" generated successfully.");
		out.close();
	}
	
	public void generatePDFPropis(File xmlFile) throws Exception {
		
		String id = "";
		String xmlFileName = xmlFile.getName();
		//System.out.println("[INFO] " + XSLFOTransformer.class.getSimpleName());
		
		// Point to the XSL-FO file
		File xsltFile = new File(PROPIS_FO_XSL_FILE_PATH);
		
		id = xmlFileName.substring(6).replace(".xml", "");
		
		// Create transformation source
		StreamSource transformSource = new StreamSource(xsltFile);
		
		// Initialize the transformation subject
		StreamSource source = new StreamSource(xmlFile);

		// Initialize user agent needed for the transformation
		FOUserAgent userAgent = fopFactory.newFOUserAgent();
		
		// Create the output stream to store the results
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		// Initialize the XSL-FO transformer object
		Transformer xslFoTransformer = transformerFactory.newTransformer(transformSource);
		
		// Construct FOP instance with desired output format
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream);

		// Resulting SAX events 
		Result res = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		xslFoTransformer.transform(source, res);

		// Generate PDF file
		File pdfFile = new File("tempFiles/propis" + id + ".pdf");
		OutputStream out = new BufferedOutputStream(new FileOutputStream(pdfFile));
		out.write(outStream.toByteArray());

		//System.out.println("[INFO] File \"" + pdfFile.getCanonicalPath() + "\" generated successfully.");
		out.close();
	}
	
	public void generatePDFAmandman(File xmlFile) throws Exception {
		
		String id = "";
		String xmlFileName = xmlFile.getName();
		//System.out.println("[INFO] " + XSLFOTransformer.class.getSimpleName());
		
		// Point to the XSL-FO file
		File xsltFile = new File(AMANDMAN_FO_XSL_FILE_PATH);
		
		id = xmlFileName.substring(8).replace(".xml", "");
		
		// Create transformation source
		StreamSource transformSource = new StreamSource(xsltFile);
		
		// Initialize the transformation subject
		StreamSource source = new StreamSource(xmlFile);

		// Initialize user agent needed for the transformation
		FOUserAgent userAgent = fopFactory.newFOUserAgent();
		
		// Create the output stream to store the results
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		// Initialize the XSL-FO transformer object
		Transformer xslFoTransformer = transformerFactory.newTransformer(transformSource);
		
		// Construct FOP instance with desired output format
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream);

		// Resulting SAX events 
		Result res = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		xslFoTransformer.transform(source, res);

		// Generate PDF file
		File pdfFile = new File("tempFiles/amandman" + id + ".pdf");
		OutputStream out = new BufferedOutputStream(new FileOutputStream(pdfFile));
		out.write(outStream.toByteArray());

		//System.out.println("[INFO] File \"" + pdfFile.getCanonicalPath() + "\" generated successfully.");
		out.close();
	}

	public static String generateHTMLPropis(File xmlFile) {
		String ret = "";
		try {
			//System.out.println("Krenuo generisanje HTML-a");
		    TransformerFactory tFactory = TransformerFactory.newInstance();

		    Transformer transformer =
		      tFactory.newTransformer
		         (new javax.xml.transform.stream.StreamSource
		            (PROPIS_XSL_FILE_PATH));

		    File tempFile = new File("htmlFile" + (new Date().getTime()));
		    StreamResult stream = new StreamResult(new FileOutputStream(tempFile));
		    
		    transformer.transform
		      (new StreamSource(xmlFile), stream);
		    //System.out.println("Generisan HTML");
		    ret = FileUtils.readFileToString(tempFile);
		    //System.out.println("html sringcina:");
		    //System.out.println(ret);
		    }
		  catch (Exception e) {
		    e.printStackTrace( );
		    }
	    return ret;
	}
	
	public static String generateHTMLAmandman(File xmlFile) {
		String ret = "";
		try {
			//System.out.println("Krenuo generisanje HTML-a");
		    TransformerFactory tFactory = TransformerFactory.newInstance();

		    Transformer transformer =
		      tFactory.newTransformer
		         (new javax.xml.transform.stream.StreamSource
		            (AMANDMAN_XSL_FILE_PATH));

		    File tempFile = new File("htmlFile" + (new Date().getTime()));
		    StreamResult stream = new StreamResult(new FileOutputStream(tempFile));
		    
		    transformer.transform
		      (new StreamSource(xmlFile), stream);
		    //System.out.println("Generisan HTML");
		    ret = FileUtils.readFileToString(tempFile);
		    //System.out.println("html sringcina:");
		    //System.out.println(ret);
		    }
		  catch (Exception e) {
		    e.printStackTrace( );
		    }
	    return ret;
	}
	
	public static void main(String[] args) throws Exception {
		new XSLFOTransformer().test();
	}

}
