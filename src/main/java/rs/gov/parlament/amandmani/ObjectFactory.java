//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5.1 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.14 at 06:40:13 PM CEST 
//


package rs.gov.parlament.amandmani;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the rs.gov.parlament.amandmani package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Amandman_QNAME = new QName("http://www.parlament.gov.rs/amandmani", "amandman");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: rs.gov.parlament.amandmani
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TAmandman }
     * 
     */
    public TAmandman createTAmandman() {
        return new TAmandman();
    }

    /**
     * Create an instance of {@link TAmandman.SadrzajAmandmana }
     * 
     */
    public TAmandman.SadrzajAmandmana createTAmandmanSadrzajAmandmana() {
        return new TAmandman.SadrzajAmandmana();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TAmandman }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/amandmani", name = "amandman")
    public JAXBElement<TAmandman> createAmandman(TAmandman value) {
        return new JAXBElement<TAmandman>(_Amandman_QNAME, TAmandman.class, null, value);
    }

}
