//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5.1 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.11 at 05:13:34 PM CEST 
//


package rs.gov.parlament.propisi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3._2000._09.xmldsig.SignatureType;
import org.w3._2001._04.xmlenc.EncryptedDataType;


/**
 * <p>Java class for TPropis complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TPropis">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2001/04/xmlenc#}EncryptedData" minOccurs="0"/>
 *         &lt;element name="naziv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.parlament.gov.rs/propisi}preambula" minOccurs="0"/>
 *         &lt;element name="potpisnik" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usvojen">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="usvojen"/>
 *               &lt;enumeration value="uNacelu"/>
 *               &lt;enumeration value="uPojedinostima"/>
 *               &lt;enumeration value="uCelini"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tekstPropisa">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://www.parlament.gov.rs/propisi}refer" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sadrzajPropisa">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element ref="{http://www.parlament.gov.rs/propisi}deo" maxOccurs="unbounded"/>
 *                   &lt;element ref="{http://www.parlament.gov.rs/propisi}glava" maxOccurs="unbounded"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="datumPredlaganja" type="{http://www.w3.org/2001/XMLSchema}date" />
 *       &lt;attribute name="datumUsvajanja" type="{http://www.w3.org/2001/XMLSchema}date" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "propis")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TPropis", propOrder = {
    "encryptedData",
    "naziv",
    "preambula",
    "potpisnik",
    "usvojen",
    "tekstPropisa",
    "sadrzajPropisa",
    "signature"
})
public class TPropis {

    @XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#")
    protected EncryptedDataType encryptedData;
    @XmlElement(required = true)
    protected String naziv;
    protected TPreambula preambula;
    @XmlElement(required = true)
    protected String potpisnik;
    @XmlElement(required = true)
    protected String usvojen;
    @XmlElement(required = true)
    protected TPropis.TekstPropisa tekstPropisa;
    @XmlElement(required = true)
    protected TPropis.SadrzajPropisa sadrzajPropisa;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "datumPredlaganja")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datumPredlaganja;
    @XmlAttribute(name = "datumUsvajanja")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datumUsvajanja;

    /**
     * Gets the value of the encryptedData property.
     * 
     * @return
     *     possible object is
     *     {@link EncryptedDataType }
     *     
     */
    public EncryptedDataType getEncryptedData() {
        return encryptedData;
    }

    /**
     * Sets the value of the encryptedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link EncryptedDataType }
     *     
     */
    public void setEncryptedData(EncryptedDataType value) {
        this.encryptedData = value;
    }

    /**
     * Gets the value of the naziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * Sets the value of the naziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaziv(String value) {
        this.naziv = value;
    }

    /**
     * Gets the value of the preambula property.
     * 
     * @return
     *     possible object is
     *     {@link TPreambula }
     *     
     */
    public TPreambula getPreambula() {
        return preambula;
    }

    /**
     * Sets the value of the preambula property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPreambula }
     *     
     */
    public void setPreambula(TPreambula value) {
        this.preambula = value;
    }

    /**
     * Gets the value of the potpisnik property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPotpisnik() {
        return potpisnik;
    }

    /**
     * Sets the value of the potpisnik property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPotpisnik(String value) {
        this.potpisnik = value;
    }

    /**
     * Gets the value of the usvojen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsvojen() {
        return usvojen;
    }

    /**
     * Sets the value of the usvojen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsvojen(String value) {
        this.usvojen = value;
    }

    /**
     * Gets the value of the tekstPropisa property.
     * 
     * @return
     *     possible object is
     *     {@link TPropis.TekstPropisa }
     *     
     */
    public TPropis.TekstPropisa getTekstPropisa() {
        return tekstPropisa;
    }

    /**
     * Sets the value of the tekstPropisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPropis.TekstPropisa }
     *     
     */
    public void setTekstPropisa(TPropis.TekstPropisa value) {
        this.tekstPropisa = value;
    }

    /**
     * Gets the value of the sadrzajPropisa property.
     * 
     * @return
     *     possible object is
     *     {@link TPropis.SadrzajPropisa }
     *     
     */
    public TPropis.SadrzajPropisa getSadrzajPropisa() {
        return sadrzajPropisa;
    }

    /**
     * Sets the value of the sadrzajPropisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPropis.SadrzajPropisa }
     *     
     */
    public void setSadrzajPropisa(TPropis.SadrzajPropisa value) {
        this.sadrzajPropisa = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     {@link SignatureType }
     *     
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *     
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the datumPredlaganja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumPredlaganja() {
        return datumPredlaganja;
    }

    /**
     * Sets the value of the datumPredlaganja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumPredlaganja(XMLGregorianCalendar value) {
        this.datumPredlaganja = value;
    }

    /**
     * Gets the value of the datumUsvajanja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumUsvajanja() {
        return datumUsvajanja;
    }

    /**
     * Sets the value of the datumUsvajanja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumUsvajanja(XMLGregorianCalendar value) {
        this.datumUsvajanja = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element ref="{http://www.parlament.gov.rs/propisi}deo" maxOccurs="unbounded"/>
     *         &lt;element ref="{http://www.parlament.gov.rs/propisi}glava" maxOccurs="unbounded"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "deo",
        "glava"
    })
    public static class SadrzajPropisa {

        protected List<TDeo> deo;
        protected List<TGlava> glava;

        /**
         * Gets the value of the deo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the deo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TDeo }
         * 
         * 
         */
        public List<TDeo> getDeo() {
            if (deo == null) {
                deo = new ArrayList<TDeo>();
            }
            return this.deo;
        }

        /**
         * Gets the value of the glava property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the glava property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGlava().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TGlava }
         * 
         * 
         */
        public List<TGlava> getGlava() {
            if (glava == null) {
                glava = new ArrayList<TGlava>();
            }
            return this.glava;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://www.parlament.gov.rs/propisi}refer" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "content"
    })
    public static class TekstPropisa {

        @XmlElementRef(name = "refer", namespace = "http://www.parlament.gov.rs/propisi", type = JAXBElement.class, required = false)
        @XmlMixed
        protected List<Serializable> content;

        /**
         * Gets the value of the content property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * {@link JAXBElement }{@code <}{@link TRefer }{@code >}
         * 
         * 
         */
        public List<Serializable> getContent() {
            if (content == null) {
                content = new ArrayList<Serializable>();
            }
            return this.content;
        }

    }

}
