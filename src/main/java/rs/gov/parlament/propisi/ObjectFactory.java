//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5.1 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.11 at 05:13:34 PM CEST 
//


package rs.gov.parlament.propisi;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the rs.gov.parlament.propisi package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Deo_QNAME = new QName("http://www.parlament.gov.rs/propisi", "deo");
    private final static QName _Glava_QNAME = new QName("http://www.parlament.gov.rs/propisi", "glava");
    private final static QName _Propis_QNAME = new QName("http://www.parlament.gov.rs/propisi", "propis");
    private final static QName _Preambula_QNAME = new QName("http://www.parlament.gov.rs/propisi", "preambula");
    private final static QName _Stav_QNAME = new QName("http://www.parlament.gov.rs/propisi", "stav");
    private final static QName _Odeljak_QNAME = new QName("http://www.parlament.gov.rs/propisi", "odeljak");
    private final static QName _Clan_QNAME = new QName("http://www.parlament.gov.rs/propisi", "clan");
    private final static QName _Tacka_QNAME = new QName("http://www.parlament.gov.rs/propisi", "tacka");
    private final static QName _Refer_QNAME = new QName("http://www.parlament.gov.rs/propisi", "refer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: rs.gov.parlament.propisi
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TDeo }
     * 
     */
    public TDeo createTDeo() {
        return new TDeo();
    }

    /**
     * Create an instance of {@link TTacka }
     * 
     */
    public TTacka createTTacka() {
        return new TTacka();
    }

    /**
     * Create an instance of {@link TGlava }
     * 
     */
    public TGlava createTGlava() {
        return new TGlava();
    }

    /**
     * Create an instance of {@link TClan }
     * 
     */
    public TClan createTClan() {
        return new TClan();
    }

    /**
     * Create an instance of {@link TPropis }
     * 
     */
    public TPropis createTPropis() {
        return new TPropis();
    }

    /**
     * Create an instance of {@link TOdeljak }
     * 
     */
    public TOdeljak createTOdeljak() {
        return new TOdeljak();
    }

    /**
     * Create an instance of {@link TStav }
     * 
     */
    public TStav createTStav() {
        return new TStav();
    }

    /**
     * Create an instance of {@link TRefer }
     * 
     */
    public TRefer createTRefer() {
        return new TRefer();
    }

    /**
     * Create an instance of {@link TPreambula }
     * 
     */
    public TPreambula createTPreambula() {
        return new TPreambula();
    }

    /**
     * Create an instance of {@link TDeo.TekstDela }
     * 
     */
    public TDeo.TekstDela createTDeoTekstDela() {
        return new TDeo.TekstDela();
    }

    /**
     * Create an instance of {@link TDeo.SadrzajDela }
     * 
     */
    public TDeo.SadrzajDela createTDeoSadrzajDela() {
        return new TDeo.SadrzajDela();
    }

    /**
     * Create an instance of {@link TTacka.TekstTacke }
     * 
     */
    public TTacka.TekstTacke createTTackaTekstTacke() {
        return new TTacka.TekstTacke();
    }

    /**
     * Create an instance of {@link TGlava.TekstGlave }
     * 
     */
    public TGlava.TekstGlave createTGlavaTekstGlave() {
        return new TGlava.TekstGlave();
    }

    /**
     * Create an instance of {@link TGlava.SadrzajGlave }
     * 
     */
    public TGlava.SadrzajGlave createTGlavaSadrzajGlave() {
        return new TGlava.SadrzajGlave();
    }

    /**
     * Create an instance of {@link TClan.TekstClana }
     * 
     */
    public TClan.TekstClana createTClanTekstClana() {
        return new TClan.TekstClana();
    }

    /**
     * Create an instance of {@link TClan.SadrzajClana }
     * 
     */
    public TClan.SadrzajClana createTClanSadrzajClana() {
        return new TClan.SadrzajClana();
    }

    /**
     * Create an instance of {@link TPropis.TekstPropisa }
     * 
     */
    public TPropis.TekstPropisa createTPropisTekstPropisa() {
        return new TPropis.TekstPropisa();
    }

    /**
     * Create an instance of {@link TPropis.SadrzajPropisa }
     * 
     */
    public TPropis.SadrzajPropisa createTPropisSadrzajPropisa() {
        return new TPropis.SadrzajPropisa();
    }

    /**
     * Create an instance of {@link TOdeljak.TekstOdeljka }
     * 
     */
    public TOdeljak.TekstOdeljka createTOdeljakTekstOdeljka() {
        return new TOdeljak.TekstOdeljka();
    }

    /**
     * Create an instance of {@link TOdeljak.SadrzajOdeljka }
     * 
     */
    public TOdeljak.SadrzajOdeljka createTOdeljakSadrzajOdeljka() {
        return new TOdeljak.SadrzajOdeljka();
    }

    /**
     * Create an instance of {@link TStav.TekstStava }
     * 
     */
    public TStav.TekstStava createTStavTekstStava() {
        return new TStav.TekstStava();
    }

    /**
     * Create an instance of {@link TStav.SadrzajStava }
     * 
     */
    public TStav.SadrzajStava createTStavSadrzajStava() {
        return new TStav.SadrzajStava();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TDeo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "deo")
    public JAXBElement<TDeo> createDeo(TDeo value) {
        return new JAXBElement<TDeo>(_Deo_QNAME, TDeo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TGlava }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "glava")
    public JAXBElement<TGlava> createGlava(TGlava value) {
        return new JAXBElement<TGlava>(_Glava_QNAME, TGlava.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TPropis }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "propis")
    public JAXBElement<TPropis> createPropis(TPropis value) {
        return new JAXBElement<TPropis>(_Propis_QNAME, TPropis.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TPreambula }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "preambula")
    public JAXBElement<TPreambula> createPreambula(TPreambula value) {
        return new JAXBElement<TPreambula>(_Preambula_QNAME, TPreambula.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TStav }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "stav")
    public JAXBElement<TStav> createStav(TStav value) {
        return new JAXBElement<TStav>(_Stav_QNAME, TStav.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TOdeljak }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "odeljak")
    public JAXBElement<TOdeljak> createOdeljak(TOdeljak value) {
        return new JAXBElement<TOdeljak>(_Odeljak_QNAME, TOdeljak.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TClan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "clan")
    public JAXBElement<TClan> createClan(TClan value) {
        return new JAXBElement<TClan>(_Clan_QNAME, TClan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TTacka }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "tacka")
    public JAXBElement<TTacka> createTacka(TTacka value) {
        return new JAXBElement<TTacka>(_Tacka_QNAME, TTacka.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRefer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.parlament.gov.rs/propisi", name = "refer")
    public JAXBElement<TRefer> createRefer(TRefer value) {
        return new JAXBElement<TRefer>(_Refer_QNAME, TRefer.class, null, value);
    }

}
